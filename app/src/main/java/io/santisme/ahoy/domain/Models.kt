package io.santisme.ahoy.domain

import androidx.core.text.HtmlCompat
import com.google.gson.annotations.SerializedName
import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*

data class Topic(
    val id: String = UUID.randomUUID().toString(),
    val title: String,
//    val date: Date = Date(),
    val date: String,
    val posts: Int = 0,
    val views: Int = 0
) {

    companion object {

        fun parseTopics(response: JSONObject): List<Topic> {
            val jsonTopics = response.getJSONObject("topic_list").getJSONArray("topics")

            val topics = mutableListOf<Topic>()

            for (i in 0 until jsonTopics.length()) {
                val parsedTopic = parseTopic(jsonTopics.getJSONObject(i))
                topics.add(parsedTopic)
            }

            return topics
        }

        fun parseTopic(jsonObject: JSONObject): Topic {

            with(jsonObject) {
                return Topic(
                    getInt("id").toString(),
                    getString("title"),
                    getString("created_at"),
                    getInt("posts_count"),
                    getInt("views")
                )
            }
        }

        const val MINUTES_MILLIS = 1000L * 60
        const val HOUR_MILLIS = MINUTES_MILLIS * 60
        const val DAY_MILLIS = HOUR_MILLIS * 24
        const val MONTH_MILLIS = DAY_MILLIS * 30
        const val YEAR_MILLIS = MONTH_MILLIS * 12
    }

    data class TimeOffset(
        val amount: Int,
        val unit: Int
    )

    fun getTimeOffset(dateToCompare: Date = Date()): TimeOffset {
        val current = dateToCompare.time

        val parser = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        val createdAt = parser.parse(this.date)

        val diff = current - createdAt.time

        val years = diff / YEAR_MILLIS
        if (years > 0) return TimeOffset(
            years.toInt(),
            Calendar.YEAR
        )

        val months = diff / MONTH_MILLIS
        if (months > 0) return TimeOffset(
            months.toInt(),
            Calendar.MONTH
        )

        val days = diff / DAY_MILLIS
        if (days > 0) return TimeOffset(
            days.toInt(),
            Calendar.DAY_OF_MONTH
        )

        val hours = diff / HOUR_MILLIS
        if (hours > 0) return TimeOffset(
            hours.toInt(),
            Calendar.HOUR
        )

        val minutes = diff / MINUTES_MILLIS
        if (minutes > 0) return TimeOffset(
            minutes.toInt(),
            Calendar.MINUTE
        )

        return TimeOffset(0, Calendar.MINUTE)
    }
}


data class Post(
    @SerializedName("topic_id") val topicId: Int,
    val id: String = UUID.randomUUID().toString(),
    val name: String,
    val username: String,
    @SerializedName("avatar_template") val avatarTemplate: String,
    @SerializedName("created_at") val createdAt: String,
    val cooked: String,
    @SerializedName("post_number") val postNumber: Int?,
    val reads: Int,
    @SerializedName("topic_title") val topicTitle: String?,
    @SerializedName("topic_slug") val topicSlug: String?,
    val score: Double?
) {

    companion object {

        fun parsePosts(response: JSONObject): List<Post> {
            val jsonPosts = response.getJSONObject("post_stream").getJSONArray("posts")

            val posts = mutableListOf<Post>()

            for (i in 0 until jsonPosts.length()) {
                val parsedTopic =
                    parsePost(
                        jsonPosts.getJSONObject(i)
                    )
                posts.add(parsedTopic)
            }

            return posts
        }

        fun parsePost(jsonObject: JSONObject): Post {

            with(jsonObject) {
                return Post(
                    getInt("topic_id"),
                    getInt("id").toString(),
                    getString("name"),
                    getString("username"),
                    getString("avatar_template"),
                    getString("created_at"),
                    HtmlCompat.fromHtml(
                        getString("cooked").toString(),
                        HtmlCompat.FROM_HTML_MODE_COMPACT
                    ).toString(),
                    null,
                    getInt("reads"),
                    null,
                    null,
                    null
                )

            }
        }

        const val MINUTES_MILLIS = 1000L * 60
        const val HOUR_MILLIS = MINUTES_MILLIS * 60
        const val DAY_MILLIS = HOUR_MILLIS * 24
        const val MONTH_MILLIS = DAY_MILLIS * 30
        const val YEAR_MILLIS = MONTH_MILLIS * 12

    }

    data class TimeOffset(
        val amount: Int,
        val unit: Int
    )


    fun getTimeOffset(dateToCompare: Date = Date()): TimeOffset {
        val current = dateToCompare.time

        val parser = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        val createdAt = parser.parse(this.createdAt)

        val diff = current - createdAt.time
        val years = diff / YEAR_MILLIS
        if (years > 0) return TimeOffset(
            years.toInt(),
            Calendar.YEAR
        )

        val months = diff / MONTH_MILLIS
        if (months > 0) return TimeOffset(
            months.toInt(),
            Calendar.MONTH
        )

        val days = diff / DAY_MILLIS
        if (days > 0) return TimeOffset(
            days.toInt(),
            Calendar.DAY_OF_MONTH
        )

        val hours = diff / HOUR_MILLIS
        if (hours > 0) return TimeOffset(
            hours.toInt(),
            Calendar.HOUR
        )

        val minutes = diff / MINUTES_MILLIS
        if (minutes > 0) return TimeOffset(
            minutes.toInt(),
            Calendar.MINUTE
        )

        return TimeOffset(0, Calendar.MINUTE)
    }
}

data class LatestNews(
    val latestNews: List<Post>
) {
    companion object {

        fun parseLatestNews(jsonObject: JSONObject): List<Post> {
            val latestPostsJson: JSONArray = jsonObject.getJSONArray("latest_posts")

            val posts = mutableListOf<Post>()

            for (i in 0 until latestPostsJson.length()) {
                val parsedPost = parseLatestNew(latestPostsJson.getJSONObject(i))
                posts.add(parsedPost)
            }

            return posts
        }

        private fun parseLatestNew(jsonObject: JSONObject): Post {

            with(jsonObject) {
                return Post(
                    getInt("topic_id"),
                    getInt("id").toString(),
                    getString("name"),
                    getString("username"),
                    getString("avatar_template"),
                    getString("created_at"),
                    getString("raw"),
                    getInt("post_number"),
                    getInt("reads"),
                    getString("topic_title"),
                    getString("topic_slug"),
                    getDouble("score")
                )

            }
        }

    }
}