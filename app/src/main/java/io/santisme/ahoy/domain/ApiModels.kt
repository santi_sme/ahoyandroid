package io.santisme.ahoy.domain

import com.google.gson.annotations.SerializedName
import org.json.JSONObject

data class SignInModel(val username: String, val password: String)

data class SignUpModel(val username: String, val email: String, val password: String) {
    fun toJson(): JSONObject {
        return JSONObject().put("name", username).put("username", username).put("email", email)
            .put("password", password).put("active", true).put("approved", true)
    }
}

data class SignUpModelWrapper(val signUpModel: SignUpModel, val confirmPassword: String)

data class CreateTopicModel(val title: String, val content: String) {
    fun toJson(): JSONObject {
        return JSONObject().put("title", title).put("raw", content)
    }
}

data class CreatePostModel(val topicId: String, val content: String, val createdAt: String) {
    fun toJson(): JSONObject {
        return JSONObject().put("topic_id", topicId).put("raw", content)
            .put("created_at", createdAt)
    }
}

data class LatestNewsModel(@SerializedName("latest_posts") val latestPosts: List<Post>) {
    fun toJson(): JSONObject {
        return JSONObject().put("latest_posts", latestPosts)
    }
}