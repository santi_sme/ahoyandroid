package io.santisme.ahoy.di

import dagger.Module
import dagger.Provides
import io.santisme.ahoy.data.repository.PostsRepo
import io.santisme.ahoy.database.PostDatabase
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
class PostsModule {

    @Singleton
    @Provides
    fun providePostsRepo(postDatabase: PostDatabase, retrofit: Retrofit): PostsRepo =
        PostsRepo.apply {
            this.db = postDatabase
            this.retrofit = retrofit
        }
}