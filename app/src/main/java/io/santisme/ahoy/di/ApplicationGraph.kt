package io.santisme.ahoy.di

import dagger.Component
import io.santisme.ahoy.feature.login.LoginContract
import io.santisme.ahoy.feature.login.view.ui.LoginActivity
import io.santisme.ahoy.feature.topic.view.ui.TopicsActivity
import javax.inject.Singleton

@Singleton
// @Component makes Dagger to create a graph of dependencies
@Component(
    modules = [LoginModule::class, LoginAbstractModule::class,
        TopicsModule::class, TopicsAbstractModule::class,
        UtilsModule::class, PostsAbstractModule::class,
        PostsModule::class]
)
interface ApplicationGraph {
    // Add functions whose return value indicating what can be provided from this container
    fun getLoginPresenter(): LoginContract.Presenter

    // Add here as well functions whose input argument is the entity in which Dagger
    // can add any dependency you want
    fun inject(loginActivity: LoginActivity)

    fun inject(topicsActivity: TopicsActivity)

}
