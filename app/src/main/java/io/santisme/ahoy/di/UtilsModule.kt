package io.santisme.ahoy.di

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import io.santisme.ahoy.BuildConfig
import io.santisme.ahoy.database.PostDatabase
import io.santisme.ahoy.database.TopicDatabase
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton


@Module
class UtilsModule(private val context: Context) {

    @Singleton
    @Provides
    fun provideApplicationContext(): Context = context

    @Singleton
    @Provides
    fun provideTopicDatabase(): TopicDatabase = Room.databaseBuilder(
        context, TopicDatabase::class.java, "topic-database"
    ).build()

    @Singleton
    @Provides
    fun providePostDatabase(): PostDatabase = Room.databaseBuilder(
        context, PostDatabase::class.java, "post-database"
    ).build()

    @Singleton
    @Provides
    fun provideRetrofit(): Retrofit = Retrofit.Builder()
        .client(provideOkHttpClient())
        .baseUrl("https://${BuildConfig.DiscourseDomain}")
        .addConverterFactory(GsonConverterFactory.create())
        .build()
}

// Inject Headers Api-Key and Api-Username using an interceptor
fun provideOkHttpClient(): OkHttpClient {

    val interceptor = object : Interceptor {
        override fun intercept(chain: Interceptor.Chain): Response {

            val original = chain.request()

            val updatedRequest = original.newBuilder()
                .header(name = "Api-Key", value = BuildConfig.DiscourseApiKey)
                .method(method = original.method, body = original.body)
                .build()

            return chain.proceed(updatedRequest)

        }

    }

    val loggingInterceptor = HttpLoggingInterceptor()
    loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

    return OkHttpClient.Builder()
        .connectTimeout(30, TimeUnit.SECONDS)
        .readTimeout(30, TimeUnit.SECONDS)
        .addInterceptor(interceptor)
//        .addInterceptor(loggingInterceptor)
        .build()
}
