package io.santisme.ahoy.di

import dagger.Module
import dagger.Provides
import io.santisme.ahoy.data.repository.TopicsRepo
import io.santisme.ahoy.database.TopicDatabase
import javax.inject.Singleton

@Module
class TopicsModule {

    @Singleton
    @Provides
    fun provideTopicsRepo(topicDatabase: TopicDatabase): TopicsRepo =
        TopicsRepo.apply { db = topicDatabase }

}
