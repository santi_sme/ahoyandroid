package io.santisme.ahoy.di

import dagger.Binds
import dagger.Module
import io.santisme.ahoy.data.repository.PostsRepo
import io.santisme.ahoy.data.repository.PostsRepository

@Module
abstract class PostsAbstractModule {

    @Binds
    abstract fun providePostsRepository(postsRepo: PostsRepo): PostsRepository

}