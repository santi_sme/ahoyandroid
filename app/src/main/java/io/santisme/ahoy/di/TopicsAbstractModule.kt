package io.santisme.ahoy.di

import dagger.Binds
import dagger.Module
import io.santisme.ahoy.data.repository.TopicsRepo
import io.santisme.ahoy.data.repository.TopicsRepository

@Module
abstract class TopicsAbstractModule {

    @Binds
    abstract fun provideTopicsRepository(topicsRepo: TopicsRepo): TopicsRepository
}
