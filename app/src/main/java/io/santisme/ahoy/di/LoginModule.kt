package io.santisme.ahoy.di

import android.content.Context
import dagger.Module
import dagger.Provides
import io.santisme.ahoy.data.repository.UserRepo
import io.santisme.ahoy.feature.login.LoginContract
import retrofit2.Retrofit
import javax.inject.Singleton

// This class allows to indicate Dagger how to resolve class implementation dependencies
@Module
class LoginModule(private val view: LoginContract.View?) {

    @Provides
    fun provideLoginContractView(): LoginContract.View? = view

    @Singleton
    @Provides
    fun providesUserRepo(context: Context, retrofit: Retrofit): UserRepo {
        UserRepo.context = context
        UserRepo.retrofit = retrofit
        return UserRepo
    }
}
