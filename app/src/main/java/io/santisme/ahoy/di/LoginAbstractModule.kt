package io.santisme.ahoy.di

import dagger.Binds
import dagger.Module
import io.santisme.ahoy.data.repository.LoginRepository
import io.santisme.ahoy.data.repository.UserRepo
import io.santisme.ahoy.feature.login.LoginContract
import io.santisme.ahoy.feature.login.presenter.LoginPresenter

// This class allows to indicate Dagger how to resolve interface implementation dependencies
@Module
abstract class LoginAbstractModule {
    @Binds
    abstract fun provideLoginContractPresenter(loginPresenter: LoginPresenter): LoginContract.Presenter

    @Binds
    abstract fun provideLoginRepository(userRepo: UserRepo): LoginRepository
}
