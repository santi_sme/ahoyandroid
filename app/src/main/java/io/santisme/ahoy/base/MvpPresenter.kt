package io.santisme.ahoy.base

interface MvpPresenter<V: MvpView> {

    fun onAttach(mvpView: V)
    fun onDetach()
}