package io.santisme.ahoy.data.service

import io.santisme.ahoy.domain.Topic
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Header

interface SingleTopicService {
    interface LatestNewsService {
        @GET("posts.json")
        suspend fun fetchSingleTopicWithCoRoutines(@Header(value = "Api-Username") headerUsername: String?): Response<Topic>
    }
}