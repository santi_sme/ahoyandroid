package io.santisme.ahoy.data.service

import io.santisme.ahoy.domain.SignInModel
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*

interface LoginService {
    @Headers("Api-Username: {username}")
    @GET("users/{username}.json")
    fun loginUser(@Path(value = "username") username: String): Call<SignInModel>

    @GET("users/{username}.json")
    suspend fun loginUserWithCoRoutines(@Path(value = "username") username: String, @Header(value = "Api-Username") headerUsername: String?): Response<SignInModel>
}