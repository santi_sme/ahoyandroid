package io.santisme.ahoy.data.repository

import android.content.Context
import android.os.Handler
import android.os.Looper
import com.android.volley.NetworkError
import com.android.volley.Request
import com.android.volley.ServerError
import io.santisme.ahoy.R
import io.santisme.ahoy.data.service.*
import io.santisme.ahoy.database.PostDatabase
import io.santisme.ahoy.database.PostEntity
import io.santisme.ahoy.domain.CreatePostModel
import io.santisme.ahoy.domain.LatestNews
import io.santisme.ahoy.domain.LatestNewsModel
import io.santisme.ahoy.domain.Post
import org.json.JSONObject
import retrofit2.Response
import retrofit2.Retrofit
import java.text.SimpleDateFormat
import java.util.*
import kotlin.concurrent.thread

object PostsRepo : PostsRepository {

    lateinit var db: PostDatabase
    lateinit var retrofit: Retrofit

    override fun getPosts(
        context: Context,
        topicId: String,
        onSuccess: (List<Post>) -> Unit,
        onError: (RequestError) -> Unit
    ) {
        val username = UserRepo.getUsername()

        val request = UserRequest(
            username,
            Request.Method.GET,
            ApiRoutes.getTopic(topicId),
            null,
            {
                it?.let {
                    onSuccess.invoke(
                        Post.parsePosts(it)
                    )
                }

                if (it == null) {
                    onError.invoke(
                        RequestError(messageResId = R.string.error_invalid_response)
                    )
                }
            },
            {
                it.printStackTrace()
                if (it is NetworkError) {
                    onError.invoke(
                        RequestError(it, messageResId = R.string.error_network)
                    )
                } else {
                    onError.invoke(RequestError(it))
                }
            })

        ApiRequestQueue.getRequestQueue(context).add(request)
    }

    override fun createPost(
        context: Context,
        model: CreatePostModel,
        onSuccess: (CreatePostModel) -> Unit,
        onError: (RequestError) -> Unit
    ) {

        val username = UserRepo.getUsername()
        val request =
            UserRequest(
                username,
                Request.Method.POST,
                ApiRoutes.createPost(),
                model.toJson(),
                {
                    it?.let {
                        onSuccess.invoke(model)
                    }

                    if (it == null)
                        onError.invoke(
                            RequestError(messageResId = R.string.error_invalid_response)
                        )

                },
                {
                    it.printStackTrace()

                    if (it is ServerError && it.networkResponse.statusCode == 442) {
                        val body = String(it.networkResponse.data, Charsets.UTF_8)
                        val jsonError = JSONObject(body)
                        val errors = jsonError.getJSONArray("errors")
                        var errorMessage = ""

                        for (i in 0 until errors.length()) errorMessage += "${errors[i]} "

                        onError.invoke(
                            RequestError(message = errorMessage)
                        )

                    } else if (it is NetworkError) {
                        onError.invoke(
                            RequestError(it, messageResId = R.string.error_network)
                        )
                    } else {
                        onError.invoke(
                            RequestError(it)
                        )
                    }
                })

        ApiRequestQueue.getRequestQueue(context).add(request)

    }

    override fun getLatestNews(
        context: Context,
        onSuccess: (List<Post>) -> Unit,
        onError: (RequestError) -> Unit
    ) {
        val username = UserRepo.getUsername()

        val request = UserRequest(
            username,
            Request.Method.GET,
            ApiRoutes.getLatestNews(),
            null,
            {
                it?.let {
                    onSuccess.invoke(LatestNews.parseLatestNews(it))
                    thread {
                        db.postDao().insertAll(postList = LatestNews.parseLatestNews(it).toEntity())
                    }
                }

                if (it == null) {
                    onError.invoke(
                        RequestError(messageResId = R.string.error_invalid_response)
                    )
                }

            },
            {
                it.printStackTrace()
                if (it is NetworkError) {
                    thread {
                        val postList = db.postDao().getPosts()
                        // Create a handler to execute code on Main Thread
                        val handler: Handler = Handler(Looper.getMainLooper())
                        handler.post {
                            Runnable {
                                if (postList.isNotEmpty()) {
                                    onSuccess(postList.toModel())
                                } else {
                                    onError.invoke(
                                        RequestError(
                                            it,
                                            messageResId = R.string.error_network
                                        )
                                    )
                                }
                            }.run()
                        }
                    }
                } else {
                    onError.invoke(RequestError(it))
                }
            })

        ApiRequestQueue.getRequestQueue(context).add(request)
    }

    override suspend fun getLatestNewsWithRetrofitSynchronouslyWithCoRoutines(username: String): Response<LatestNewsModel> =
        retrofit.create(LatestNewsService::class.java)
            .fetchLatestNewsWithCoRoutines(headerUsername = username)

}

private fun List<PostEntity>.toModel(): List<Post> = map { it.toModel() }

private fun PostEntity.toModel(): Post {
//    val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ", Locale.getDefault())
//    val dateFormatted = dateFormat.parse(createdAt) ?: Date()

    return Post(
        topicId = topicId,
        id = id,
        name = name,
        username = username,
        avatarTemplate = avatarTemplate,
        createdAt = createdAt,
        cooked = cooked,
        postNumber = postNumber,
        reads = reads,
        topicTitle = topicTitle,
        topicSlug = topicSlug,
        score = score
    )
}

private fun List<Post>.toEntity(): List<PostEntity> = map { it.toEntity() }

private fun Post.toEntity(): PostEntity = PostEntity(
    topicId = topicId,
    id = id,
    name = name,
    username = username,
    avatarTemplate = avatarTemplate,
    createdAt = createdAt,
    cooked = cooked,
    postNumber = postNumber,
    reads = reads,
    topicTitle = topicTitle,
    topicSlug = topicSlug,
    score = score
)