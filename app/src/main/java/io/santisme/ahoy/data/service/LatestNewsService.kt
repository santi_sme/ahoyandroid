package io.santisme.ahoy.data.service

import io.santisme.ahoy.domain.LatestNewsModel
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Header

interface LatestNewsService {
    @GET("posts.json")
    suspend fun fetchLatestNewsWithCoRoutines(@Header(value = "Api-Username") headerUsername: String?): Response<LatestNewsModel>
}