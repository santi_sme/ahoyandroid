package io.santisme.ahoy.data.service

import android.net.Uri
import io.santisme.ahoy.BuildConfig

object ApiRoutes {

    fun signIn(username: String) =
        uriBuilder().appendPath("users").appendPath("${username}.json").build().toString()

    fun signUp() =
        uriBuilder().appendPath("users").build().toString()

    fun getTopics() =
        uriBuilder().appendPath("latest.json").build().toString()

    fun getTopic(topicId: String) =
        uriBuilder().appendPath("t").appendPath("${topicId}.json").build().toString()

    private fun uriBuilder() =
        Uri.Builder().scheme("https").authority(BuildConfig.DiscourseDomain)

    fun createTopic() =
        uriBuilder().appendPath("posts.json").build().toString()

    fun createPost() = createTopic()

    fun getLatestNews() =
        uriBuilder().appendPath("posts.json").build().toString()
}