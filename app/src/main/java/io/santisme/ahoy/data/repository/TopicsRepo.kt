package io.santisme.ahoy.data.repository

import android.content.Context
import android.os.Handler
import android.os.Looper
import com.android.volley.NetworkError
import com.android.volley.Request
import com.android.volley.ServerError
import io.santisme.ahoy.R
import io.santisme.ahoy.data.service.ApiRequestQueue
import io.santisme.ahoy.data.service.ApiRoutes
import io.santisme.ahoy.data.service.RequestError
import io.santisme.ahoy.data.service.UserRequest
import io.santisme.ahoy.database.TopicDatabase
import io.santisme.ahoy.database.TopicEntity
import io.santisme.ahoy.domain.CreateTopicModel
import io.santisme.ahoy.domain.Topic
import org.json.JSONObject
import kotlin.concurrent.thread

object TopicsRepo : TopicsRepository {

    lateinit var db: TopicDatabase

    override fun getTopics(
        context: Context,
        onSuccess: (List<Topic>) -> Unit,
        onError: (RequestError) -> Unit
    ) {
        val username = UserRepo.getUsername()

        val request = UserRequest(
            username,
            Request.Method.GET,
            ApiRoutes.getTopics(),
            null,
            {
                it?.let {
                    onSuccess.invoke(Topic.parseTopics(it))
                    thread {
                        db.topicDao().insertAll(topicList = Topic.parseTopics(it).toEntity())
                    }
                }

                if (it == null) {
                    onError.invoke(
                        RequestError(messageResId = R.string.error_invalid_response)
                    )
                }
            },
            {
                it.printStackTrace()
                if (it is NetworkError) {
                    thread {
                        val topicList = db.topicDao().getTopics()
                        // Create a handler to execute code on Main Thread
                        val handler: Handler = Handler(Looper.getMainLooper())
                        handler.post {
                            Runnable {
                                if (topicList.isNotEmpty()) {
                                    onSuccess(topicList.toModel())
                                } else {
                                    onError.invoke(
                                        RequestError(
                                            it,
                                            messageResId = R.string.error_network
                                        )
                                    )
                                }
                            }.run()
                        }
                    }

                } else {
                    onError.invoke(RequestError(it))
                }
            })

        ApiRequestQueue.getRequestQueue(context).add(request)
    }


    override fun createTopic(
        context: Context,
        model: CreateTopicModel,
        onSuccess: (CreateTopicModel) -> Unit,
        onError: (RequestError) -> Unit
    ) {
        val username = UserRepo.getUsername()
        val request =
            UserRequest(
                username,
                Request.Method.POST,
                ApiRoutes.createTopic(),
                model.toJson(),
                {
                    it?.let {
                        onSuccess.invoke(model)
                    }

                    if (it == null) {
                        onError.invoke(RequestError(messageResId = R.string.error_invalid_response))
                    }

                },
                {
                    it.printStackTrace()

                    if (it is ServerError && it.networkResponse.statusCode == 442) {
                        val body = String(it.networkResponse.data, Charsets.UTF_8)
                        val jsonError = JSONObject(body)
                        val errors = jsonError.getJSONArray("errors")
                        var errorMessage = ""

                        for (i in 0 until errors.length()) errorMessage += "${errors[i]} "

                        onError.invoke(
                            RequestError(message = errorMessage)
                        )

                    } else if (it is NetworkError) {
                        onError.invoke(
                            RequestError(it, messageResId = R.string.error_network)
                        )
                    } else {
                        onError.invoke(
                            RequestError(it)
                        )
                    }
                })

        ApiRequestQueue.getRequestQueue(context).add(request)
    }
}

private fun List<TopicEntity>.toModel(): List<Topic> = map { it.toModel() }

private fun TopicEntity.toModel(): Topic = Topic(
    id = topicId,
    title = title,
    date = date,
    posts = posts,
    views = views
)

private fun List<Topic>.toEntity(): List<TopicEntity> = map { it.toEntity() }

private fun Topic.toEntity(): TopicEntity = TopicEntity(
    topicId = id,
    title = title,
    date = date,
    posts = posts,
    views = views
)
