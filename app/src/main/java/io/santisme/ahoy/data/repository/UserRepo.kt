package io.santisme.ahoy.data.repository

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.os.Handler
import com.android.volley.NetworkError
import com.android.volley.Request
import com.android.volley.ServerError
import io.santisme.ahoy.R
import io.santisme.ahoy.data.service.*
import io.santisme.ahoy.domain.SignInModel
import io.santisme.ahoy.domain.SignUpModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import kotlin.concurrent.thread


const val PREFERENCES_SESSION = "session"
const val PREFERENCES_SESSION_USERNAME = "username"

object UserRepo : LoginRepository {

    lateinit var context: Context
    lateinit var retrofit: Retrofit

    override fun signIn(
        signInModel: SignInModel,
        onSuccess: (SignInModel) -> Unit,
        onError: (RequestError) -> Unit
    ) {

        val request = AdminRequest(
            Request.Method.GET,
            ApiRoutes.signIn(signInModel.username),
            null,
            {
                onSuccess.invoke(signInModel)
            }, { error ->
                error.printStackTrace()

                if (error is ServerError && error.networkResponse.statusCode == 404)
                    onError.invoke(
                        RequestError(
                            error,
                            messageResId = R.string.error_not_registered
                        )
                    )
                else if (error is NetworkError) {
                    onError.invoke(
                        RequestError(
                            error,
                            messageResId = R.string.error_network
                        )
                    )
                } else {
                    onError.invoke(
                        RequestError(
                            error
                        )
                    )
                }
            })

        ApiRequestQueue.getRequestQueue(context)
            .add(request)

    }

    override fun signInWithRetrofitSynchronously(
        signInModel: SignInModel,
        onSuccess: (SignInModel) -> Unit,
        onError: (RequestError) -> Unit
    ) {

        thread {
            val response: Response<SignInModel> =
                retrofit.create(LoginService::class.java).loginUser(signInModel.username).execute()

            val handler = Handler(context.mainLooper)

            val runnable = Runnable {
                if (response.isSuccessful) {
                    response.body().takeIf { it != null }?.let(onSuccess) ?: run {
                        onError(RequestError(message = "Body is null"))
                    }
                } else {
                    onError(RequestError(message = response.errorBody().toString()))
                }

            }
            handler.post(runnable)
        }
    }

    override fun signInWithRetrofitAsynchronously(
        signInModel: SignInModel,
        onSuccess: (SignInModel) -> Unit,
        onError: (RequestError) -> Unit
    ) {
        retrofit.create(LoginService::class.java).loginUser(signInModel.username)
            .enqueue(object : Callback<SignInModel> {
                override fun onFailure(call: Call<SignInModel>, t: Throwable) {
                    onError(RequestError(message = t.localizedMessage))
                }

                override fun onResponse(call: Call<SignInModel>, response: Response<SignInModel>) {
                    if (response.isSuccessful) {
                        response.body().takeIf { it != null }?.let(onSuccess) ?: run {
                            onError(RequestError(message = "Body is null"))
                        }
                    } else {
                        onError(RequestError(message = response.errorBody().toString()))
                    }
                }

            })

    }

    override suspend fun signInWithRetrofitSynchronouslyWithCoRoutines(signInModel: SignInModel): Response<SignInModel> =
        retrofit.create(LoginService::class.java).loginUserWithCoRoutines(
            username = signInModel.username,
            headerUsername = signInModel.username
        )

    override fun signUp(
        model: SignUpModel,
        onSuccess: (SignUpModel) -> Unit,
        onError: (RequestError) -> Unit
    ) {
        val request =
            AdminRequest(
                Request.Method.POST,
                ApiRoutes.signUp(),
                model.toJson(),
                { response ->

                    response?.let {
                        if (it.optBoolean("success")) {
                            onSuccess.invoke(model)
                        } else
                            onError.invoke(
                                RequestError(
                                    message = it.getString("message")
                                )
                            )

                    }

                    if (response == null) {
                        onError.invoke(
                            RequestError(
                                messageResId = R.string.error_invalid_response
                            )
                        )
                    }
                },
                { error ->

                    error.printStackTrace()

                    if (error is NetworkError) {
                        onError.invoke(
                            RequestError(
                                error,
                                messageResId = R.string.error_network
                            )
                        )
                    } else {
                        onError.invoke(
                            RequestError(
                                error
                            )
                        )
                    }

                })

        ApiRequestQueue.getRequestQueue(context)
            .add(request)
    }

    override fun isLogged(): Boolean {
        val pref =
            context.applicationContext.getSharedPreferences(
                PREFERENCES_SESSION, MODE_PRIVATE
            )
        val username = pref.getString(PREFERENCES_SESSION_USERNAME, null)
        return username != null
    }

    fun getUsername(): String {
        val pref =
            context.applicationContext.getSharedPreferences(
                PREFERENCES_SESSION, MODE_PRIVATE
            )
        return pref.getString(PREFERENCES_SESSION_USERNAME, "") ?: ""
    }

    override fun saveSession(username: String) {
        val pref =
            context.applicationContext.getSharedPreferences(
                PREFERENCES_SESSION, MODE_PRIVATE
            )
        pref.edit().putString(PREFERENCES_SESSION_USERNAME, username).apply()

    }

    fun logOut() {
        val pref =
            context.applicationContext.getSharedPreferences(
                PREFERENCES_SESSION, MODE_PRIVATE
            )
        pref.edit().remove(PREFERENCES_SESSION_USERNAME).apply()
    }
}