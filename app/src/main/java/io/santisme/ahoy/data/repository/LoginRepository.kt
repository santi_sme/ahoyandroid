package io.santisme.ahoy.data.repository

import io.santisme.ahoy.data.service.RequestError
import io.santisme.ahoy.domain.SignInModel
import io.santisme.ahoy.domain.SignUpModel
import retrofit2.Call
import retrofit2.Response

interface LoginRepository {
    fun signIn(
        signInModel: SignInModel,
        onSuccess: (SignInModel) -> Unit, onError: (RequestError) -> Unit
    )

    fun signInWithRetrofitSynchronously(
        signInModel: SignInModel,
        onSuccess: (SignInModel) -> Unit, onError: (RequestError) -> Unit
    )

    fun signInWithRetrofitAsynchronously(
        signInModel: SignInModel,
        onSuccess: (SignInModel) -> Unit, onError: (RequestError) -> Unit
    )

    suspend fun signInWithRetrofitSynchronouslyWithCoRoutines(signInModel: SignInModel): Response<SignInModel>

    fun signUp(
        model: SignUpModel,
        onSuccess: (SignUpModel) -> Unit, onError: (RequestError) -> Unit
    )

    fun isLogged(): Boolean
    fun saveSession(username: String)
}