package io.santisme.ahoy.data.repository

import android.content.Context
import io.santisme.ahoy.data.service.RequestError
import io.santisme.ahoy.domain.CreateTopicModel
import io.santisme.ahoy.domain.Topic

interface TopicsRepository {
    fun getTopics(
        context: Context, onSuccess: (List<Topic>) -> Unit,
        onError: (RequestError) -> Unit
    )

    fun createTopic(
        context: Context, model: CreateTopicModel,
        onSuccess: (CreateTopicModel) -> Unit, onError: (RequestError) -> Unit
    )
}