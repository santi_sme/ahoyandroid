package io.santisme.ahoy.data.repository

import android.content.Context
import io.santisme.ahoy.data.service.RequestError
import io.santisme.ahoy.domain.CreatePostModel
import io.santisme.ahoy.domain.LatestNewsModel
import io.santisme.ahoy.domain.Post
import retrofit2.Response

interface PostsRepository {
    fun getPosts(
        context: Context,
        topicId: String,
        onSuccess: (List<Post>) -> Unit,
        onError: (RequestError) -> Unit
    )

    fun createPost(
        context: Context,
        model: CreatePostModel,
        onSuccess: (CreatePostModel) -> Unit,
        onError: (RequestError) -> Unit
    )

    fun getLatestNews(
        context: Context,
        onSuccess: (List<Post>) -> Unit,
        onError: (RequestError) -> Unit
    )

    suspend fun getLatestNewsWithRetrofitSynchronouslyWithCoRoutines(username: String): Response<LatestNewsModel>

}