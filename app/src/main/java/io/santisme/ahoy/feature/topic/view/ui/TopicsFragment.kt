package io.santisme.ahoy.feature.topic.view.ui

import android.content.Context
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import io.santisme.ahoy.R
import io.santisme.ahoy.data.service.RequestError
import io.santisme.ahoy.domain.Topic
import io.santisme.ahoy.feature.topic.view.adapter.TopicsAdapter
import kotlinx.android.synthetic.main.dialog_loading.view.*
import kotlinx.android.synthetic.main.fragment_topics.*
import kotlinx.android.synthetic.main.view_retry.*

const val TOPICS_FRAGMENT_TAG = "TOPICS_FRAGMENT"

class TopicsFragment : Fragment() {

    private var listener: TopicsInteractionListener? = null
    private lateinit var topicsAdapter: TopicsAdapter

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is TopicsInteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement ${TopicsInteractionListener::class.java.simpleName}")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setHasOptionsMenu(true)
        topicsAdapter = TopicsAdapter {
            onTopicSelected(it)
        }

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_topics, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_topics, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        listTopics.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
            adapter = topicsAdapter
        }

        viewLoading.labelMessage.text = "Loading"

        swiperefresh.setOnRefreshListener {
            swiperefresh.isRefreshing = false
            listener?.onRefreshListener()
        }

        buttonCreate.setOnClickListener {
            createTopicButtonClicked()
        }

        buttonRetry.setOnClickListener {
            retryButtonClicked()
        }
    }

    override fun onResume() {
        super.onResume()
        listener?.onTopicsFragmentResumed()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_logout -> listener?.onLogOutOptionClicked()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun retryButtonClicked() {
        listener?.onRetryButtonClicked()

    }

    fun enableLoading(enabled: Boolean) {
        viewRetry.visibility = View.INVISIBLE
        if (enabled) {
            listTopics.visibility = View.INVISIBLE
            buttonCreate.hide()
            viewLoading.visibility = View.VISIBLE
        } else {
            listTopics.visibility = View.VISIBLE
            buttonCreate.show()
            viewLoading.visibility = View.INVISIBLE
        }
    }

    fun handleRequestError(requestError: RequestError) {
        listTopics.visibility = View.INVISIBLE
        buttonCreate.hide()
        viewRetry.visibility = View.VISIBLE

        val message = if (requestError.messageResId != null)
            getString(requestError.messageResId)
        else requestError.message ?: getString(R.string.error_request_default)

        Snackbar.make(parentLayout, message, Snackbar.LENGTH_LONG).show()
    }

    private fun createTopicButtonClicked() {
        listener?.onCreateTopicButtonClicked()
    }

    private fun onTopicSelected(topic: Topic) {
        listener?.onTopicSelected(topic)
    }

    fun loadTopics(topicList: List<Topic>) {
        enableLoading(false)
        topicsAdapter.setTopics(topicList)
    }

    interface TopicsInteractionListener {
        fun onTopicSelected(topic: Topic)
        fun onCreateTopicButtonClicked()
        fun onLogOutOptionClicked()
        fun onTopicsFragmentResumed()
        fun onRefreshListener()
        fun onRetryButtonClicked()
    }

}
