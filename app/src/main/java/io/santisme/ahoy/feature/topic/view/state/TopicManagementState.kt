package io.santisme.ahoy.feature.topic.view.state

import io.santisme.ahoy.data.service.RequestError
import io.santisme.ahoy.domain.Topic

sealed class TopicManagementState {
    object Loading : TopicManagementState()
    object NavigateToCreateTopic : TopicManagementState()
    object NavigateToLogin : TopicManagementState()
    object NavigateBackToTopics : TopicManagementState()
    object NavigateToTopics : TopicManagementState()
    object NavigateToLatestNews : TopicManagementState()

    class LoadTopicList(val topicList: List<Topic>) : TopicManagementState()
    class RequestErrorReported(val requestError: RequestError) : TopicManagementState()
    class CreateTopicFormErrorReported(val msg: String) : TopicManagementState()
    class NavigateToPosts(val topic: Topic) : TopicManagementState()
    class TopicCreatedSuccessfully(val msg: String) : TopicManagementState()
    class TopicNotCreated(val createError: String) : TopicManagementState()
    class ToggleCreateTopicLoading(val enabled: Boolean) : TopicManagementState()

}
