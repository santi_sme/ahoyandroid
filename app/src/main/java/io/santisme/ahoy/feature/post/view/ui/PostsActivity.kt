package io.santisme.ahoy.feature.post.view.ui


import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import io.santisme.ahoy.R
import kotlinx.android.synthetic.main.toolbar_posts.*

const val EXTRA_TOPIC_ID = "topic_id"
const val EXTRA_TOPIC_TITLE = "topic_title"
const val TRANSACTION_CREATE_POST = "create_post"

class PostsActivity : AppCompatActivity(),
    PostsFragment.PostsInteractionListener,
    CreatePostFragment.CreatePostInteractionListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_posts)

        val topicId = intent.getStringExtra(EXTRA_TOPIC_ID)
        val topicTitle = intent.getStringExtra(EXTRA_TOPIC_TITLE)

        initView()

        if (topicId != null && topicId.isNotEmpty()) {

            if (savedInstanceState == null) {
                supportFragmentManager.beginTransaction()
                    .add(
                        R.id.fragmentContainer,
                        PostsFragment(
                            topicId,
                            topicTitle
                        ), POSTS_FRAGMENT_TAG
                    )
                    .commit()
            }

        } else {
            throw IllegalArgumentException("You should provide an id for the topic")
        }
    }

    override fun onCreatePost() {
        supportFragmentManager.popBackStack()
    }

    override fun onGoToCreatePost(topicId: String, topicTitle: String) {
        supportFragmentManager.beginTransaction()
            .replace(
                R.id.fragmentContainer,
                CreatePostFragment(
                    topicId,
                    topicTitle
                )
            )
            .addToBackStack(TRANSACTION_CREATE_POST).commit()
    }

    private fun initView() {
        setSupportActionBar(toolbarPostsToolbar)
        // TODO: Implement SearchView
    }
}
