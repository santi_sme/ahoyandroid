package io.santisme.ahoy.feature.post.view.ui

import android.content.Context
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import io.santisme.ahoy.feature.LoadingDialogFragment
import io.santisme.ahoy.R
import io.santisme.ahoy.domain.CreatePostModel
import io.santisme.ahoy.data.repository.PostsRepo
import io.santisme.ahoy.data.service.RequestError
import kotlinx.android.synthetic.main.fragment_create_post.*
import java.util.*

class CreatePostFragment(val topicId: String, val topicTitle: String): Fragment() {

    var listener: CreatePostInteractionListener? = null
    lateinit var loadingDialog: LoadingDialogFragment

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is CreatePostInteractionListener)
            listener = context
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)

        loadingDialog = LoadingDialogFragment.newInstance(getString(R.string.label_create_post))

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_create_post, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        labelTopic.text = topicTitle

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_create_post, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_send -> createPost()
        }

        return super.onOptionsItemSelected(item)
    }

    private fun createPost() {
        if (isFormValid()) {
            postPost()
        } else {
            showErrors()
        }
    }

    private fun postPost() {
        val createdAt = Date()
        val model = CreatePostModel(
            topicId,
            inputContent.text.toString(),
            createdAt.toString()
        )
        context?.let {
            enableLoadingDialog(true)
            PostsRepo.createPost(it, model, {
                enableLoadingDialog(false)
                listener?.onCreatePost()
            }, {
                enableLoadingDialog(false)
                handleError(it)
            })
        }
    }

    private fun enableLoadingDialog(enabled: Boolean) {
        if (enabled)
            loadingDialog.show(childFragmentManager,
                TAG_LOADING_DIALOG
            )
        else
            loadingDialog.dismiss()

    }

    private fun handleError(requestError: RequestError) {
        val message = if (requestError.messageResId != null)
            getString(requestError.messageResId)
        else requestError.message ?: getString(R.string.error_request_default)

        Snackbar.make(parentLayout, message, Snackbar.LENGTH_LONG).show()
    }

    private fun isFormValid() =
        inputContent.text?.let {
            it.isNotEmpty() && it.length >= 20
        } ?: false

    private fun showErrors() {
        inputContent.text?.let {
            if (it.isEmpty())
                inputContent.error = getString(R.string.error_empty)
            if (it.length < 20)
                inputContent.error = getString(R.string.error_post_length)

        }

    }

    interface CreatePostInteractionListener {
        fun onCreatePost()
    }

}