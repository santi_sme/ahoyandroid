package io.santisme.ahoy.feature.login.view.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import io.santisme.ahoy.R
import io.santisme.ahoy.data.service.RequestError
import io.santisme.ahoy.di.DaggerApplicationGraph
import io.santisme.ahoy.di.LoginModule
import io.santisme.ahoy.di.UtilsModule
import io.santisme.ahoy.domain.SignInModel
import io.santisme.ahoy.domain.SignUpModelWrapper
import io.santisme.ahoy.feature.login.LoginContract
import io.santisme.ahoy.feature.topic.view.ui.TopicsActivity
import kotlinx.android.synthetic.main.activity_login.*
import javax.inject.Inject

class LoginActivity : AppCompatActivity(), LoginContract.View,
    SignInFragment.SignInInteractionListener,
    SignUpFragment.SignUpInteractionListener {

    private val signInFragment: SignInFragment by lazy { SignInFragment.newInstance() }
    private val signUpFragment: SignUpFragment by lazy { SignUpFragment.newInstance() }

    @Inject
    lateinit var loginPresenter: LoginContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        // Initialize Dagger component and inject dependencies into this entity
        DaggerApplicationGraph.builder()
            .loginModule(LoginModule(view = this))
            .utilsModule(UtilsModule(context = applicationContext))
            .build()
            .inject(loginActivity = this)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        if (savedInstanceState == null) {
            loginPresenter.onViewCreatedWithNoSavedData()
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        loginPresenter.onDetach()
    }

    override fun onCreateNewAccountClicked() {
        loginPresenter.onCreateNewAccountClicked()

    }

    override fun onLoginButtonClicked(signInModel: SignInModel) {
//        loginPresenter.onLoginClicked(context = this, model = signInModel)
        loginPresenter.onLoginClickedForCoRoutines(context = this, model = signInModel)

    }

    override fun onLabelSignClicked() {
        loginPresenter.onLabelSignClicked()

    }

    override fun onSignUpButtonClicked(signUpModelWrapper: SignUpModelWrapper) {
        loginPresenter.onSignUpClicked(context = this, modelWrapper = signUpModelWrapper)

    }

    override fun showError(msg: String) {
        Snackbar.make(parentLayout, msg, Snackbar.LENGTH_LONG).show()
    }

    override fun toggleSigningOption() {
        val currentFragment = supportFragmentManager.findFragmentByTag(SIGN_IN_FRAGMENT_TAG)
        val newFragmentWrapper: Pair<Fragment, String> =
            if (currentFragment != null && currentFragment.isVisible) {
                signUpFragment to SIGN_UP_FRAGMENT_TAG
            } else {
                signInFragment to SIGN_IN_FRAGMENT_TAG
            }

        supportFragmentManager.beginTransaction()
            .replace(R.id.fragmentContainer, newFragmentWrapper.first, newFragmentWrapper.second)
            .commit()
    }

    override fun navigateToSignIn() {
        supportFragmentManager.beginTransaction()
            .replace(R.id.fragmentContainer, signInFragment, SIGN_IN_FRAGMENT_TAG)
            .commit()
    }

    override fun handleRequestError(requestError: RequestError) {
        val message = if (requestError.messageResId != null)
            getString(requestError.messageResId)
        else requestError.message ?: getString(R.string.error_request_default)

        Snackbar.make(parentLayout, message, Snackbar.LENGTH_LONG).show()
    }

    override fun enableLoading(enable: Boolean) {
        if (enable) {
            fragmentContainer.visibility = View.INVISIBLE
            viewLoading.visibility = View.VISIBLE
        } else {
            fragmentContainer.visibility = View.VISIBLE
            viewLoading.visibility = View.INVISIBLE
        }

    }

    private fun simulateLoading() {
        val runnable = Runnable {
            Thread.sleep(3000)
            viewLoading.post {
                launchTopicsActivity()
            }
        }

        Thread(runnable).start()
    }

    override fun launchTopicsActivity() {
        val intent = Intent(this, TopicsActivity::class.java)
        startActivity(intent)
        finish()
    }

}
