package io.santisme.ahoy.feature.login.presenter

import android.content.Context
import io.santisme.ahoy.R
import io.santisme.ahoy.data.repository.LoginRepository
import io.santisme.ahoy.data.service.RequestError
import io.santisme.ahoy.domain.SignInModel
import io.santisme.ahoy.domain.SignUpModelWrapper
import io.santisme.ahoy.feature.login.LoginContract
import io.santisme.ahoy.feature.login.view.ui.MIN_PASS_LENGTH
import kotlinx.coroutines.*
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext

// This constructor needs to be annotated with '@Inject', so Dagger can use it
class LoginPresenter @Inject constructor(
    private var view: LoginContract.View?, private var userRepo: LoginRepository
) : LoginContract.Presenter, CoroutineScope {

    private val job: CompletableJob = Job()
    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.IO

    override fun onAttach(mvpView: LoginContract.View) {
        // No needed to define it since 'view' is already injected through constructor
    }

    override fun onDetach() {
        view = null
    }

    override fun onViewCreatedWithNoSavedData() {
        if (userRepo.isLogged()) {
            view?.launchTopicsActivity()
        } else {
            view?.navigateToSignIn()
        }
    }

    override fun onCreateNewAccountClicked() {
        view?.toggleSigningOption()
    }

    override fun onLabelSignClicked() {
        view?.toggleSigningOption()
    }

    override fun onLoginClicked(context: Context, model: SignInModel) {
        if (isValidForm(signInModel = model)) {
            view?.enableLoading(true)
            userRepo.signInWithRetrofitAsynchronously(model, {
                userRepo.saveSession(model.username)
                view?.enableLoading(false)
                view?.launchTopicsActivity()
            }, {
                view?.enableLoading(false)
                view?.handleRequestError(it)
            })
        } else {
            view?.showError(msg = getSigInFormError(context = context, signInModel = model))
        }
    }

    override fun onLoginClickedForCoRoutines(context: Context, model: SignInModel) {
        if (isValidForm(signInModel = model)) {
            view?.enableLoading(true)

            val job = async {
                userRepo.signInWithRetrofitSynchronouslyWithCoRoutines(signInModel = model)

            }

            launch(Dispatchers.Main) {
                val response = job.await()

                view?.enableLoading(false)
                if (response.isSuccessful) {
                    response.body().takeIf { it != null }?.let {
                        userRepo.saveSession(model.username)
                        view?.launchTopicsActivity()
                    }
                        ?: run { view?.handleRequestError(requestError = RequestError(message = "Body is null")) }
                } else {
                    view?.handleRequestError(RequestError(message = response.errorBody().toString()))
                }
            }
        } else {
            view?.showError(msg = getSigInFormError(context = context, signInModel = model))
        }
    }

    override fun onSignUpClicked(context: Context, modelWrapper: SignUpModelWrapper) {
        if (isValidSignUpForm(signUpModelWrapper = modelWrapper)) {
            view?.enableLoading(true)
            userRepo.signUp(modelWrapper.signUpModel, {
                userRepo.saveSession(modelWrapper.signUpModel.username)
                view?.enableLoading(false)
                view?.launchTopicsActivity()
            }, {
                view?.enableLoading(false)
                view?.handleRequestError(it)
            })
        } else {
            view?.showError(
                msg = getSigUpFormError(
                    context = context,
                    signUpModelWrapper = modelWrapper
                )
            )
        }

    }

    private fun getSigInFormError(context: Context, signInModel: SignInModel): String {
        return with(signInModel) {
            when {
                username.isEmpty() -> context.getString(R.string.error_username_empty)
                password.isEmpty() -> context.getString(R.string.error_password_empty)
                else -> context.getString(R.string.error_unknown)
            }
        }
    }

    private fun getSigUpFormError(
        context: Context,
        signUpModelWrapper: SignUpModelWrapper
    ): String {
        return with(signUpModelWrapper) {
            with(signUpModel) {
                when {
                    email.isEmpty() -> context.getString(R.string.error_email_empty)
                    username.isEmpty() -> context.getString(R.string.error_username_empty)
                    password.isEmpty() -> context.getString(R.string.error_password_empty)
                    password.length < MIN_PASS_LENGTH -> context.getString(R.string.error_password_length)
                    password != confirmPassword -> context.getString(R.string.error_password_match)
                    else -> context.getString(R.string.error_unknown)
                }
            }
        }
    }

    private fun isValidSignUpForm(signUpModelWrapper: SignUpModelWrapper): Boolean =
        with(signUpModelWrapper) {
            signUpModel.email.isNotEmpty() && signUpModel.username.isNotEmpty()
                    && signUpModel.password.isNotEmpty() && signUpModel.password.length >= MIN_PASS_LENGTH
                    && signUpModel.password == confirmPassword
        }

    private fun isValidForm(signInModel: SignInModel) =
        with(signInModel) {
            username.isNotEmpty() && password.isNotEmpty()
        }
}
