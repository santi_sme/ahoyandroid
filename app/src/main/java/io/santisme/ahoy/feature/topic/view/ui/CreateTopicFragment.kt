package io.santisme.ahoy.feature.topic.view.ui

import android.content.Context
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import io.santisme.ahoy.R
import io.santisme.ahoy.domain.CreateTopicModel
import io.santisme.ahoy.data.repository.UserRepo
import io.santisme.ahoy.feature.LoadingDialogFragment
import kotlinx.android.synthetic.main.fragment_create_topic.*

const val TAG_LOADING_DIALOG = "loading_dialog"
const val CREATE_TOPIC_FRAGMENT_TAG = "CREATE_TOPIC_FRAGMENT"

class CreateTopicFragment : Fragment() {

    private var listener: CreateTopicInteractionListener? = null
    private lateinit var loadingDialog: LoadingDialogFragment

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is CreateTopicInteractionListener) {
            listener = context

        } else {
            throw RuntimeException("$context must implement ${CreateTopicInteractionListener::class.java.simpleName}")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)

        loadingDialog = LoadingDialogFragment.newInstance(getString(R.string.label_create_topic))
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_create_topic, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        labelAuthor.text = UserRepo.getUsername()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_create_topic, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_send -> listener?.onCreateTopicOptionClicked(
                createTopicModel = CreateTopicModel(
                    inputTitle.text.toString(),
                    inputContent.text.toString()
                )
            )
        }

        return super.onOptionsItemSelected(item)
    }

    fun enableLoadingDialog(enabled: Boolean) {
        if (enabled) {
            loadingDialog.show(childFragmentManager, TAG_LOADING_DIALOG)
        } else {
            loadingDialog.dismiss()
        }

    }

    interface CreateTopicInteractionListener {
        fun onCreateTopicOptionClicked(createTopicModel: CreateTopicModel)
    }

}
