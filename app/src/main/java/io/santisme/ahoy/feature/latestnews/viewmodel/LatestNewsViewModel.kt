package io.santisme.ahoy.feature.latestnews.viewmodel

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.santisme.ahoy.data.repository.PostsRepository
import io.santisme.ahoy.data.repository.UserRepo
import io.santisme.ahoy.data.service.RequestError
import io.santisme.ahoy.domain.Post
import io.santisme.ahoy.feature.latestnews.state.LatestNewsManagementState
import kotlinx.coroutines.*
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext

class LatestNewsViewModel @Inject constructor(
    private val postsRepo: PostsRepository,
    private val userRepo: UserRepo
) :
    ViewModel(), CoroutineScope {

    private val job: CompletableJob = Job()
    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.IO

    private lateinit var _latestNewsManagementState: MutableLiveData<LatestNewsManagementState>
    val latestNewsManagementState: LiveData<LatestNewsManagementState>
        get() {
            if (!::_latestNewsManagementState.isInitialized) {
                _latestNewsManagementState = MutableLiveData()
            }

            return _latestNewsManagementState
        }

    fun onPostSelected(post: Post) {
        _latestNewsManagementState.value =
            LatestNewsManagementState.OnNavigateToTopicDetail(post)
    }

    private fun fetchLatestNewsAndHandleResponse(context: Context?) {
        context?.let {
            postsRepo.getLatestNews(it, { postList ->
                _latestNewsManagementState.value =
                    LatestNewsManagementState.LoadLatestNews(list = postList)
            }, { error ->
                _latestNewsManagementState.value =
                    LatestNewsManagementState.RequestErrorReported(requestError = error)
            })
        }
    }

    private fun fetchLatestNewsAndHandleResponseWithCoRoutines(username: String) {
        val job = async {
            postsRepo.getLatestNewsWithRetrofitSynchronouslyWithCoRoutines(username = username)
        }

        launch(Dispatchers.Main) {
            val response = job.await()

            _latestNewsManagementState.value =
                LatestNewsManagementState.ToggleLatestNewsLoading(enabled = true)

            if (response.isSuccessful) {
                response.body().takeIf { it != null }?.let { postList ->
                    _latestNewsManagementState.value =
                        LatestNewsManagementState.LoadLatestNews(list = postList.latestPosts)
                }
                    ?: run {
                        _latestNewsManagementState.value =
                            LatestNewsManagementState.RequestErrorReported(
                                requestError = RequestError(
                                    message = "Body is null"
                                )
                            )
                    }
            } else {
                _latestNewsManagementState.value =
                    LatestNewsManagementState.RequestErrorReported(
                        requestError = RequestError(message = response.errorBody().toString())
                    )
            }
        }
    }

    fun onRetryButtonClicked() {
        _latestNewsManagementState.value =
            LatestNewsManagementState.ToggleLatestNewsLoading(enabled = true)
        fetchLatestNewsAndHandleResponseWithCoRoutines(username = userRepo.getUsername())
    }

    fun onLatestNewsResumed() {
        _latestNewsManagementState.value =
            LatestNewsManagementState.ToggleLatestNewsLoading(enabled = true)
        fetchLatestNewsAndHandleResponseWithCoRoutines(username = userRepo.getUsername())
    }

    fun onRefreshLatestNewsListener() {
        _latestNewsManagementState.value =
            LatestNewsManagementState.ToggleLatestNewsLoading(enabled = true)
        fetchLatestNewsAndHandleResponseWithCoRoutines(username = userRepo.getUsername())
    }

}