package io.santisme.ahoy.feature.latestnews.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import io.santisme.ahoy.BuildConfig
import io.santisme.ahoy.R
import io.santisme.ahoy.domain.Post
import kotlinx.android.synthetic.main.item_latest_news.view.*
import java.util.*

class LatestNewsPostHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    var post: Post? = null
        set(value) {
            field = value
            with(itemView) {
                tag = field
                field?.let {
                    labelAuthor.text = it.name
                    labelTopicTitle.text = it.topicTitle
                    labelTopicSlug.text = it.topicSlug
                    labelDate.text = it.createdAt
                    setTimeOffset(it.getTimeOffset())
                    labelPostNumber.text = "Post number ${it.postNumber}"
                    labelScore.text = "Score ${it.score}"
                    Glide
                        .with(this)
                        .load(
                            "https://${BuildConfig.DiscourseDomain}${it.avatarTemplate.replace(
                                "{size}",
                                "64"
                            )}"
                        )
                        .placeholder(R.drawable.ic_user_avatar_placeholder)
                        .circleCrop()
                        .into(userAvatar)

                }
            }
        }

    private fun setTimeOffset(timeOffset: Post.TimeOffset) {
        val quantityString = when (timeOffset.unit) {
            Calendar.YEAR -> R.plurals.years
            Calendar.MONTH -> R.plurals.months
            Calendar.DAY_OF_MONTH -> R.plurals.days
            Calendar.HOUR -> R.plurals.hours
            Calendar.MINUTE -> R.plurals.minutes
            else -> R.plurals.years
        }

        itemView.labelDate.text = if (timeOffset.amount != 0)
            itemView.context.resources.getQuantityString(
                quantityString,
                timeOffset.amount,
                timeOffset.amount
            )
        else
            itemView.context.resources.getString(R.string.minutes_zero)
    }
}
