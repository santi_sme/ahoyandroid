package io.santisme.ahoy.feature.topic.view.ui

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.Observer
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.Snackbar
import io.santisme.ahoy.R
import io.santisme.ahoy.data.service.RequestError
import io.santisme.ahoy.di.DaggerApplicationGraph
import io.santisme.ahoy.di.LoginModule
import io.santisme.ahoy.di.PostsModule
import io.santisme.ahoy.di.UtilsModule
import io.santisme.ahoy.domain.CreateTopicModel
import io.santisme.ahoy.domain.Post
import io.santisme.ahoy.domain.Topic
import io.santisme.ahoy.feature.latestnews.state.LatestNewsManagementState
import io.santisme.ahoy.feature.latestnews.view.LATEST_NEWS_FRAGMENT_TAG
import io.santisme.ahoy.feature.latestnews.view.LatestNewsFragment
import io.santisme.ahoy.feature.latestnews.viewmodel.LatestNewsViewModel
import io.santisme.ahoy.feature.login.view.ui.LoginActivity
import io.santisme.ahoy.feature.post.view.ui.*
import io.santisme.ahoy.feature.topic.view.state.TopicManagementState
import io.santisme.ahoy.feature.topic.viewmodel.TopicViewModel
import kotlinx.android.synthetic.main.activity_topics.*
import kotlinx.android.synthetic.main.toolbar_topics.*
import javax.inject.Inject

const val TRANSACTION_CREATE_TOPIC = "create_topic"
const val EMPTY = ""

class TopicsActivity @Inject constructor() : AppCompatActivity(),
    TopicsFragment.TopicsInteractionListener,
    CreateTopicFragment.CreateTopicInteractionListener,
    NavigationView.OnNavigationItemSelectedListener,
    LatestNewsFragment.LatestNewsInteractionListener {

    @Inject
    lateinit var topicViewModel: TopicViewModel
    @Inject
    lateinit var latestNewsViewModel: LatestNewsViewModel

    private lateinit var drawer: DrawerLayout
    private lateinit var toggle: ActionBarDrawerToggle

    override fun onCreate(savedInstanceState: Bundle?) {
        DaggerApplicationGraph.builder()
            .loginModule(LoginModule(view = null))
            .utilsModule(UtilsModule(context = applicationContext))
            .postsModule(PostsModule())
            .build()
            .inject(topicsActivity = this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_topics)

        initTopicsModel()
        initLatestNewsModel()
        initView()

        if (savedInstanceState == null) {
            topicViewModel.onCreatedWithNoSavedData(this)
            supportFragmentManager.beginTransaction()
                .add(
                    R.id.fragmentContainer,
                    TopicsFragment(), TOPICS_FRAGMENT_TAG
                )
                .commit()
        }
    }

    override fun onTopicSelected(topic: Topic) {
        topicViewModel.onTopicSelected(topic = topic)
    }

    override fun onCreateTopicButtonClicked() {
        topicViewModel.onCreateTopicButtonClicked()
    }

    override fun onLogOutOptionClicked() {
        topicViewModel.onLogOutOptionClicked()

    }

    override fun onTopicsFragmentResumed() {
        topicViewModel.onTopicsFragmentResumed(context = this)
    }

    override fun onRefreshListener() {
        topicViewModel.onRefreshListener(context = this)
    }

    override fun onRetryButtonClicked() {
        topicViewModel.onRetryButtonClicked(context = this)
    }

    override fun onCreateTopicOptionClicked(createTopicModel: CreateTopicModel) {
        topicViewModel.onCreateTopicOptionClicked(
            context = this,
            createTopicModel = createTopicModel
        )
    }

    override fun onNavigationItemSelected(p0: MenuItem): Boolean {
        when (p0.itemId) {
            R.id.action_topics -> topicViewModel.onNavigationTopicsItemSelected()
            R.id.action_latest_news -> topicViewModel.onNavigationLatestNewsItemSelected()
        }
        drawer.closeDrawers()
        return super.onOptionsItemSelected(p0)
    }

    private fun initTopicsModel() {
        topicViewModel.topicManagementState.observe(this, Observer { state ->
            when (state) {
                TopicManagementState.Loading -> enableLoadingView()
                is TopicManagementState.LoadTopicList -> loadTopicList(list = state.topicList)
                is TopicManagementState.RequestErrorReported -> showRequestError(requestError = state.requestError)
                is TopicManagementState.NavigateToCreateTopic -> navigateToCreateTopic()
                is TopicManagementState.NavigateToLogin -> navigateToLoginAndExit()
                is TopicManagementState.TopicCreatedSuccessfully -> showMessage(msg = state.msg)
                is TopicManagementState.TopicNotCreated -> showError(msg = state.createError)
                is TopicManagementState.CreateTopicFormErrorReported -> showError(msg = state.msg)
                is TopicManagementState.NavigateToPosts -> navigateToPosts(topic = state.topic)
                is TopicManagementState.NavigateBackToTopics -> navigateBackToTopics()
                is TopicManagementState.ToggleCreateTopicLoading -> enableCreateTopicLoadingView(
                    enabled = state.enabled
                )
                is TopicManagementState.NavigateToTopics -> navigateToTopics()
                is TopicManagementState.NavigateToLatestNews -> navigateToLatestNews()

            }
        })
    }

    private fun initView() {
        setSupportActionBar(toolbarTopicsToolbar)

        drawer = drawer_layout
        toggle = ActionBarDrawerToggle(
            this,
            drawer,
            toolbarTopicsToolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )

        // This method shows the burger symbol in the toolbar
        toggle.syncState()
        drawer.addDrawerListener(toggle)
        navigation_view.setNavigationItemSelectedListener(this)

        toolbarTopicsSearchView.apply {

            setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean {
                    topicViewModel.onSearchViewQuerySubmit(context = context, query = query)
                    return true
                }

                override fun onQueryTextChange(newText: String?): Boolean {
                    return true
                }

            })
            setOnCloseListener {
                topicViewModel.onSearchViewQuerySubmit(context = context, query = EMPTY)
                false
            }
        }
    }

    private fun enableCreateTopicLoadingView(enabled: Boolean) {
        getCreateTopicFragmentIfAvailableOrNull()?.enableLoadingDialog(enabled = enabled)
        getTopicsFragmentIfAvailableOrNull()?.enableLoading(enabled = enabled)
    }

    private fun navigateBackToTopics() {
        supportFragmentManager.popBackStack()

    }

    private fun navigateToLoginAndExit() {
        startActivity(Intent(this, LoginActivity::class.java))
        finish()
    }

    private fun navigateToCreateTopic() {
        supportFragmentManager.beginTransaction()
            .replace(
                R.id.fragmentContainer,
                CreateTopicFragment(), CREATE_TOPIC_FRAGMENT_TAG
            )
            .addToBackStack(TRANSACTION_CREATE_TOPIC).commit()
    }

    private fun navigateToPosts(topic: Topic) {
        val intent = Intent(this, PostsActivity::class.java)
        intent.putExtra(EXTRA_TOPIC_ID, topic.id)
        intent.putExtra(EXTRA_TOPIC_TITLE, topic.title)

        startActivity(intent)
    }

    private fun navigateToLatestNews() {
        if (getLatestNewsFragmentIfAvailableOrNull() == null) {
            supportFragmentManager.beginTransaction()
                .replace(
                    R.id.fragmentContainer,
                    LatestNewsFragment(),
                    LATEST_NEWS_FRAGMENT_TAG
                )
                .commit()
        }
    }

    private fun navigateToTopics() {
        if (getTopicsFragmentIfAvailableOrNull() == null) {
            supportFragmentManager.beginTransaction()
                .replace(
                    R.id.fragmentContainer,
                    TopicsFragment(), TOPICS_FRAGMENT_TAG
                )
                .commit()
        }
    }

    private fun showRequestError(requestError: RequestError) {
        getTopicsFragmentIfAvailableOrNull()?.run {
            enableLoading(enabled = false)
            handleRequestError(requestError = requestError)
        }
    }

    private fun enableLoadingView() {
        getTopicsFragmentIfAvailableOrNull()?.enableLoading(enabled = true)

    }

    private fun loadTopicList(list: List<Topic>) {
        getTopicsFragmentIfAvailableOrNull()?.run {
            enableLoading(enabled = false)
            loadTopics(topicList = list)
        }
    }

    private fun getTopicsFragmentIfAvailableOrNull(): TopicsFragment? {
        val currentFragment = supportFragmentManager.findFragmentByTag(TOPICS_FRAGMENT_TAG)
        return if (currentFragment != null && currentFragment.isVisible) {
            currentFragment as TopicsFragment
        } else {
            null
        }

    }

    private fun getCreateTopicFragmentIfAvailableOrNull(): CreateTopicFragment? {
        val currentFragment = supportFragmentManager.findFragmentByTag(CREATE_TOPIC_FRAGMENT_TAG)
        return if (currentFragment != null && currentFragment.isVisible) {
            currentFragment as CreateTopicFragment
        } else {
            null
        }

    }

    private fun showError(msg: String) {
        Snackbar.make(activityTopicsRoot, msg, Snackbar.LENGTH_LONG).show()
    }

    private fun showMessage(msg: String) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show()
    }


    // MARK: LatestNews Methods
    override fun onPostSelected(post: Post) {
        latestNewsViewModel.onPostSelected(post = post)
    }

    override fun onRefreshLatestNewsListener() {
        latestNewsViewModel.onRefreshLatestNewsListener()
    }

    override fun onLatestNewsRetryButtonClicked() {
        latestNewsViewModel.onRetryButtonClicked()
    }

    override fun onLatestNewsResumed() {
        latestNewsViewModel.onLatestNewsResumed()
    }

    private fun initLatestNewsModel() {
        latestNewsViewModel.latestNewsManagementState.observe(this, Observer { state ->
            when (state) {
                is LatestNewsManagementState.LoadLatestNews -> loadLatestNewsList(list = state.list)
                is LatestNewsManagementState.RequestErrorReported -> showLatestNewsRequestError(
                    requestError = state.requestError
                )
                is LatestNewsManagementState.ToggleLatestNewsLoading -> enableLoadingLatestNewsLoadingView(
                    enabled = state.enabled
                )
                is LatestNewsManagementState.OnNavigateToTopicDetail -> onNavigateToTopicDetail(
                    post = state.post
                )

            }
        })
    }

    private fun onNavigateToTopicDetail(post: Post) {
        val intent = Intent(this, PostsActivity::class.java)
        intent.putExtra(EXTRA_TOPIC_ID, post.topicId.toString())
        intent.putExtra(EXTRA_TOPIC_TITLE, post.topicTitle)

        startActivity(intent)
//        supportFragmentManager.beginTransaction()
//            .replace(
//                R.id.fragmentContainer,
//                PostsFragment(post.topicId.toString(), post.topicTitle ?: ""), POSTS_FRAGMENT_TAG
//            )
//            .commit()
    }

    private fun getLatestNewsFragmentIfAvailableOrNull(): LatestNewsFragment? {
        val currentFragment = supportFragmentManager.findFragmentByTag(LATEST_NEWS_FRAGMENT_TAG)
        return if (currentFragment != null && currentFragment.isVisible) {
            currentFragment as LatestNewsFragment
        } else {
            null
        }

    }

    private fun loadLatestNewsList(list: List<Post>) {
        getLatestNewsFragmentIfAvailableOrNull()?.run {
            enableLoading(enabled = false)
            loadLatestNews(postList = list)
        }
    }

    private fun showLatestNewsRequestError(requestError: RequestError) {
        getLatestNewsFragmentIfAvailableOrNull()?.run {
            enableLoading(enabled = false)
            handleRequestError(requestError = requestError)
        }
    }

    private fun enableLoadingLatestNewsLoadingView(enabled: Boolean) {
        getLatestNewsFragmentIfAvailableOrNull()?.enableLoading(enabled = enabled)

    }

}