package io.santisme.ahoy.feature.topic.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import io.santisme.ahoy.R
import io.santisme.ahoy.domain.Topic
import io.santisme.ahoy.feature.topic.view.viewholder.TopicHolder

class TopicsAdapter(
    val topicClickListener: ((Topic) -> Unit)? = null
) : RecyclerView.Adapter<TopicHolder>() {

    private val topicList = mutableListOf<Topic>()

    private val listener: ((View) -> Unit) = {
        val topic = it.tag as Topic
        topicClickListener?.invoke(topic)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TopicHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_topic, parent, false)

        return TopicHolder(view)
    }

    override fun getItemCount(): Int {
        return topicList.size
    }

    override fun onBindViewHolder(holder: TopicHolder, position: Int) {
        val topic = topicList[position]
        holder.topic = topic
        holder.itemView.setOnClickListener(listener)
    }

    fun setTopics(topics: List<Topic>) {
        this.topicList.clear()
        this.topicList.addAll(topics)
        notifyDataSetChanged()
    }

}
