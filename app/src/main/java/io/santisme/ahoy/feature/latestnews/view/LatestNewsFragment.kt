package io.santisme.ahoy.feature.latestnews.view

import android.content.Context
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import io.santisme.ahoy.R
import io.santisme.ahoy.data.service.RequestError
import io.santisme.ahoy.domain.Post
import io.santisme.ahoy.feature.latestnews.adapter.LatestNewsAdapter
import kotlinx.android.synthetic.main.dialog_loading.view.*
import kotlinx.android.synthetic.main.fragment_latest_news.*
import kotlinx.android.synthetic.main.view_retry.*
import javax.inject.Inject


const val LATEST_NEWS_FRAGMENT_TAG = "latest_news_fragment"

class LatestNewsFragment @Inject constructor() : Fragment() {

    lateinit var adapter: LatestNewsAdapter
    private var listener: LatestNewsInteractionListener? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is LatestNewsInteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement ${LatestNewsInteractionListener::class.java.simpleName}")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setHasOptionsMenu(true)
        adapter = LatestNewsAdapter {
            onPostSelected(it)
        }

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_topics, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_latest_news, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        listLatestNews.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        listLatestNews.adapter = adapter

        viewLoading.labelMessage.text = "Loading"

        swiperefresh.setOnRefreshListener {
            swiperefresh.isRefreshing = false
            listener?.onRefreshLatestNewsListener()
        }

        buttonRetry.setOnClickListener {
            retryButtonClicked()
        }

    }

    override fun onResume() {
        super.onResume()
        listener?.onLatestNewsResumed()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_logout -> listener?.onLogOutOptionClicked()
        }
        return super.onOptionsItemSelected(item)
    }

    fun enableLoading(enabled: Boolean) {
        viewRetry.visibility = View.INVISIBLE
        if (enabled) {
            listLatestNews.visibility = View.INVISIBLE
            viewLoading.visibility = View.VISIBLE
        } else {
            listLatestNews.visibility = View.VISIBLE
            viewLoading.visibility = View.INVISIBLE
        }
    }

    fun loadLatestNews(postList: List<Post>) {
        enableLoading(false)
        adapter.setPosts(postList)
    }

    fun handleRequestError(requestError: RequestError) {
        listLatestNews.visibility = View.INVISIBLE
        viewRetry.visibility = View.VISIBLE

        val message = if (requestError.messageResId != null)
            getString(requestError.messageResId)
        else requestError.message ?: getString(R.string.error_request_default)

        Snackbar.make(LatestNewsParentLayout, message, Snackbar.LENGTH_LONG).show()
    }

    private fun onPostSelected(post: Post) {
        listener?.onPostSelected(post)
    }

    private fun retryButtonClicked() {
        listener?.onLatestNewsRetryButtonClicked()
    }

    interface LatestNewsInteractionListener {
        fun onPostSelected(post: Post)
        fun onRefreshLatestNewsListener()
        fun onLatestNewsRetryButtonClicked()
        fun onLatestNewsResumed()
        fun onLogOutOptionClicked()
    }
}