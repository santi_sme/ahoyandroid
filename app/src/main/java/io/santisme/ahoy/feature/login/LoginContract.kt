package io.santisme.ahoy.feature.login

import android.content.Context
import io.santisme.ahoy.base.MvpPresenter
import io.santisme.ahoy.base.MvpView
import io.santisme.ahoy.data.service.RequestError
import io.santisme.ahoy.domain.SignInModel
import io.santisme.ahoy.domain.SignUpModelWrapper

interface LoginContract {

    interface View : MvpView {
        fun launchTopicsActivity()
        fun navigateToSignIn()
        fun enableLoading(enable: Boolean)
        fun handleRequestError(requestError: RequestError)
        fun showError(msg: String)
        fun toggleSigningOption()

    }

    interface Presenter : MvpPresenter<View> {
        fun onViewCreatedWithNoSavedData()
        fun onCreateNewAccountClicked()
        fun onLoginClicked(context: Context, model: SignInModel)
        fun onLoginClickedForCoRoutines(context: Context, model: SignInModel)
        fun onLabelSignClicked()
        fun onSignUpClicked(context: Context, modelWrapper: SignUpModelWrapper)

    }
}