package io.santisme.ahoy.feature.login.view.ui

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import io.santisme.ahoy.R
import io.santisme.ahoy.domain.SignUpModel
import io.santisme.ahoy.domain.SignUpModelWrapper
import kotlinx.android.synthetic.main.fragment_sign_up.*

const val MIN_PASS_LENGTH: Int = 10
const val SIGN_UP_FRAGMENT_TAG = "SIGN_UP_FRAGMENT"

class SignUpFragment : Fragment() {

    private var listener: SignUpInteractionListener? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)

        if (context is SignUpInteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement ${SignUpInteractionListener::class.java.simpleName}")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_sign_up, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        buttonSignUp.setOnClickListener { signUpButtonClicked() }
        labelSignIn.setOnClickListener { labelSignInClicked() }

    }

    private fun labelSignInClicked() {
        listener?.onLabelSignClicked()
    }

    private fun signUpButtonClicked() {
        listener?.onSignUpButtonClicked(
            SignUpModelWrapper(
                SignUpModel(
                    username = inputUsername.text.toString(),
                    email = inputEmail.text.toString(),
                    password = inputPassword.text.toString()
                ), confirmPassword = inputConfirmPassword.text.toString()
            )
        )

    }

    interface SignUpInteractionListener {
        fun onLabelSignClicked()
        fun onSignUpButtonClicked(signUpModelWrapper: SignUpModelWrapper)
    }

    private fun isValidForm() =
        inputEmail.text?.isNotEmpty() ?: false
                && inputUsername.text?.isNotEmpty() ?: false
                && inputPassword.text?.isNotEmpty() ?: false
                && inputPassword.text?.length ?: 0 >= MIN_PASS_LENGTH
                && inputUsername.text?.isNotEmpty() ?: false
                && inputEmail.text.toString() == inputConfirmPassword.text.toString()

    private fun showFormErrors() {
        if (inputEmail.text?.isEmpty() == true)
            inputEmail.error = getString(R.string.error_empty)
        if (inputUsername.text?.isEmpty() == true)
            inputUsername.error = getString(R.string.error_empty)
        if (inputPassword.text?.isEmpty() == true)
            inputPassword.error = getString(R.string.error_empty)
        if (inputPassword.text?.length ?: 0 < MIN_PASS_LENGTH)
            inputPassword.error = getString(R.string.error_password_length)
        if (inputConfirmPassword.text?.isEmpty() == true)
            inputConfirmPassword.error = getString(R.string.error_empty)
        if (inputPassword.text.toString() != inputConfirmPassword.text.toString())
            inputConfirmPassword.error = getString(R.string.error_password_match)

    }

    companion object {
        fun newInstance() =
            SignUpFragment()

    }
}