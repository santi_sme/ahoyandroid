package io.santisme.ahoy.feature.post.view.ui

import android.content.Context
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import io.santisme.ahoy.R
import io.santisme.ahoy.data.repository.PostsRepo
import io.santisme.ahoy.data.service.RequestError
import io.santisme.ahoy.feature.post.view.adapter.PostsAdapter
import kotlinx.android.synthetic.main.dialog_loading.view.*
import kotlinx.android.synthetic.main.fragment_posts.*

const val TAG_LOADING_DIALOG = "loading_dialog"
const val POSTS_FRAGMENT_TAG = "posts_fragment"

class PostsFragment(val topicId: String, val topicTitle: String) : Fragment() {

    lateinit var adapter: PostsAdapter
    var listener: PostsInteractionListener? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is PostsInteractionListener)
            listener = context
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        adapter = PostsAdapter()

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_posts, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        listPosts.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        listPosts.adapter = adapter

        viewLoading.labelMessage.text = "Loading"

        swiperefresh.setOnRefreshListener {
            swiperefresh.isRefreshing = false
            loadPosts(topicId)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_posts, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_create_topic -> goToCreatePost()
        }

        return super.onOptionsItemSelected(item)
    }

    private fun goToCreatePost() {
        listener?.onGoToCreatePost(topicId, topicTitle)
    }

    override fun onResume() {
        super.onResume()
        loadPosts(topicId)
    }

    private fun loadPosts(topicId: String) {
        enableLoading(true)
        context?.let {
            PostsRepo.getPosts(it, topicId, { listPosts ->
                enableLoading(false)
                adapter.setPosts(listPosts)
            }, { error ->
                enableLoading(false)
                handleRequestError(error)
            })
        }
    }

    private fun enableLoading(enabled: Boolean) {
        viewRetry.visibility = View.INVISIBLE
        if (enabled) {
            listPosts.visibility = View.INVISIBLE
            viewLoading.visibility = View.VISIBLE
        } else {
            listPosts.visibility = View.VISIBLE
            viewLoading.visibility = View.INVISIBLE
        }
    }

    private fun handleRequestError(requestError: RequestError) {
        listPosts.visibility = View.INVISIBLE
        viewRetry.visibility = View.VISIBLE

        val message = if (requestError.messageResId != null)
            getString(requestError.messageResId)
        else requestError.message ?: getString(R.string.error_request_default)

        Snackbar.make(parentLayout, message, Snackbar.LENGTH_LONG).show()
    }

    interface PostsInteractionListener {
        fun onGoToCreatePost(topicId: String, topicTitle: String)
    }
}
