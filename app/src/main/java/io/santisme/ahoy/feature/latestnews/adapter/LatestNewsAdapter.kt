package io.santisme.ahoy.feature.latestnews.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import io.santisme.ahoy.R
import io.santisme.ahoy.domain.Post
import io.santisme.ahoy.feature.latestnews.viewholder.LatestNewsPostHolder

class LatestNewsAdapter(
    val postClickListener: ((Post) -> Unit)? = null
) : RecyclerView.Adapter<LatestNewsPostHolder>() {

    private val postList = mutableListOf<Post>()

    private val listener: ((View) -> Unit) = {
        val post = it.tag as Post
        postClickListener?.invoke(post)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LatestNewsPostHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_latest_news, parent, false)

        return LatestNewsPostHolder(view)
    }

    override fun getItemCount(): Int {
        return postList.size
    }

    override fun onBindViewHolder(holder: LatestNewsPostHolder, position: Int) {
        val post = postList[position]
        holder.post = post
        holder.itemView.setOnClickListener(listener)
    }

    fun setPosts(posts: List<Post>) {
        this.postList.clear()
        this.postList.addAll(posts)
        notifyDataSetChanged()
    }

}