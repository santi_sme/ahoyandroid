package io.santisme.ahoy.feature.post.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import io.santisme.ahoy.BuildConfig
import io.santisme.ahoy.R
import io.santisme.ahoy.domain.Post
import kotlinx.android.synthetic.main.item_post.view.*
import java.util.*

class PostsAdapter() : RecyclerView.Adapter<PostsAdapter.PostHolder>() {

    private val posts = mutableListOf<Post>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_post, parent, false)

        return PostHolder(view)
    }

    override fun getItemCount(): Int {
        return posts.size
    }

    override fun onBindViewHolder(holder: PostHolder, position: Int) {
        val post = posts[position]
        holder.post = post
    }

    fun setPosts(posts: List<Post>) {
        this.posts.clear()
        this.posts.addAll(posts)
        notifyDataSetChanged()
    }

    inner class PostHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var post: Post? = null
            set(value) {
                field = value
                with(itemView) {
                    tag = field
                    field?.let {
                        labelAuthor.text = it.name
                        labelContent.text = it.cooked
                        labelDate.text = it.createdAt
                        setTimeOffset(it.getTimeOffset())
                        Glide
                            .with(this)
                            .load(
                                "https://${BuildConfig.DiscourseDomain}${it.avatarTemplate.replace(
                                    "{size}",
                                    "64"
                                )}"
                            )
                            .placeholder(R.drawable.ic_user_avatar_placeholder)
                            .circleCrop()
                            .into(userAvatar)

                    }
                }
            }

        private fun setTimeOffset(timeOffset: Post.TimeOffset) {
            val quantityString = when (timeOffset.unit) {
                Calendar.YEAR -> R.plurals.years
                Calendar.MONTH -> R.plurals.months
                Calendar.DAY_OF_MONTH -> R.plurals.days
                Calendar.HOUR -> R.plurals.hours
                Calendar.MINUTE -> R.plurals.minutes
                else -> R.plurals.years
            }

            itemView.labelDate.text = if (timeOffset.amount != 0)
                itemView.context.resources.getQuantityString(
                    quantityString,
                    timeOffset.amount,
                    timeOffset.amount
                )
            else
                itemView.context.resources.getString(R.string.minutes_zero)
        }
    }

}