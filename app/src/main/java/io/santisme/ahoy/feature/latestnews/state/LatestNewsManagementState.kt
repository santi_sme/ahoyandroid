package io.santisme.ahoy.feature.latestnews.state

import io.santisme.ahoy.data.service.RequestError
import io.santisme.ahoy.domain.Post

sealed class LatestNewsManagementState {
    class OnNavigateToTopicDetail(val post: Post) : LatestNewsManagementState()
    class RequestErrorReported(val requestError: RequestError) : LatestNewsManagementState()
    class LoadLatestNews(val list: List<Post>) : LatestNewsManagementState()
    class ToggleLatestNewsLoading(val enabled: Boolean) : LatestNewsManagementState()

}
