package io.santisme.ahoy.feature.login.view.ui

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import io.santisme.ahoy.R
import io.santisme.ahoy.domain.SignInModel
import kotlinx.android.synthetic.main.fragment_sign_in.*
import kotlinx.android.synthetic.main.fragment_sign_in.view.*

const val SIGN_IN_FRAGMENT_TAG = "SIGN_IN_FRAGMENT"

class SignInFragment : Fragment() {

    private var listener: SignInInteractionListener? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)

        if (context is SignInInteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement ${SignInInteractionListener::class.java.simpleName}")
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_sign_in, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(view) {
            labelCreateAccount.setOnClickListener {
                labelCreateNewAccountClicked()
            }

            buttonLogin.setOnClickListener {
                loginButtonClicked()
            }
        }
    }

    private fun loginButtonClicked() {
        listener?.onLoginButtonClicked(
            SignInModel(
                inputUsername.text.toString(),
                inputPassword.text.toString()
            )
        )

    }

    private fun labelCreateNewAccountClicked() {
        listener?.onCreateNewAccountClicked()
    }

    interface SignInInteractionListener {
        fun onCreateNewAccountClicked()
        fun onLoginButtonClicked(signInModel: SignInModel)
    }

//    private fun showFormErrors() {
//        if (inputUsername.text?.isEmpty() == true)
//            inputUsername.error = getString(R.string.error_empty)
//        if (inputPassword.text?.isEmpty() == true)
//            inputPassword.error = getString(R.string.error_empty)
//    }

    companion object {
        fun newInstance() =
            SignInFragment()

    }
}