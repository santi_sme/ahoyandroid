package io.santisme.ahoy.feature.topic.viewmodel

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.santisme.ahoy.R
import io.santisme.ahoy.data.repository.TopicsRepository
import io.santisme.ahoy.data.repository.UserRepo
import io.santisme.ahoy.domain.CreateTopicModel
import io.santisme.ahoy.domain.Topic
import io.santisme.ahoy.feature.topic.view.state.TopicManagementState
import javax.inject.Inject

class TopicViewModel @Inject constructor(private val topicsRepo: TopicsRepository) : ViewModel() {

    private lateinit var _topicManagementState: MutableLiveData<TopicManagementState>
    val topicManagementState: LiveData<TopicManagementState>
        get() {
            if (!::_topicManagementState.isInitialized) {
                _topicManagementState = MutableLiveData()
            }

            return _topicManagementState
        }

    fun onCreatedWithNoSavedData(context: Context?) {
        _topicManagementState.value = TopicManagementState.ToggleCreateTopicLoading(enabled = true)
        fetchTopicsAndHandleResponse(context = context)

    }

    fun onTopicsFragmentResumed(context: Context) {
        _topicManagementState.value = TopicManagementState.ToggleCreateTopicLoading(enabled = true)
        fetchTopicsAndHandleResponse(context = context)

    }

    fun onRetryButtonClicked(context: Context) {
        _topicManagementState.value = TopicManagementState.ToggleCreateTopicLoading(enabled = true)
        fetchTopicsAndHandleResponse(context = context)

    }

    fun onRefreshListener(context: Context) {
        _topicManagementState.value = TopicManagementState.ToggleCreateTopicLoading(enabled = true)
        fetchTopicsAndHandleResponse(context = context)

    }

    fun onTopicSelected(topic: Topic) {
        _topicManagementState.value = TopicManagementState.NavigateToPosts(topic = topic)
    }

    fun onCreateTopicButtonClicked() {
        _topicManagementState.value = TopicManagementState.NavigateToCreateTopic
    }

    fun onLogOutOptionClicked() {
        UserRepo.logOut()
        _topicManagementState.value = TopicManagementState.NavigateToLogin

    }

    fun onCreateTopicOptionClicked(context: Context, createTopicModel: CreateTopicModel) {
        if (isFormValidForm(createTopicModel = createTopicModel)) {
            _topicManagementState.value =
                TopicManagementState.ToggleCreateTopicLoading(enabled = true)

            topicsRepo.createTopic(context, createTopicModel, { topicModel ->

                if (topicModel == createTopicModel) {
                    _topicManagementState.value =
                        TopicManagementState.ToggleCreateTopicLoading(enabled = false)
                    _topicManagementState.value =
                        TopicManagementState.TopicCreatedSuccessfully(msg = context.getString((R.string.message_topic_successfully_created)))
                    _topicManagementState.value = TopicManagementState.NavigateBackToTopics
                } else {
                    _topicManagementState.value =
                        TopicManagementState.ToggleCreateTopicLoading(enabled = false)
                    _topicManagementState.value =
                        TopicManagementState.TopicNotCreated(createError = context.getString(R.string.error_topic_not_created))
                }

            }, { requestError ->
                _topicManagementState.value =
                    TopicManagementState.ToggleCreateTopicLoading(enabled = false)
                _topicManagementState.value =
                    TopicManagementState.RequestErrorReported(requestError = requestError)
            })

        } else {
            _topicManagementState.value = TopicManagementState.CreateTopicFormErrorReported(
                msg = getCreateTopicFormError(
                    context = context,
                    createTopicModel = createTopicModel
                )
            )
        }
    }

    private fun fetchTopicsAndHandleResponse(context: Context?) {
        context?.let {
            topicsRepo.getTopics(it, { topicList ->
                _topicManagementState.value =
                    TopicManagementState.LoadTopicList(topicList = topicList)
            }, { error ->
                _topicManagementState.value =
                    TopicManagementState.RequestErrorReported(requestError = error)
            })
        }
    }

    private fun isFormValidForm(createTopicModel: CreateTopicModel): Boolean =
        with(createTopicModel) {
            title.isNotEmpty() && content.isNotEmpty()
        }

    private fun getCreateTopicFormError(
        context: Context,
        createTopicModel: CreateTopicModel
    ): String {
        return with(createTopicModel) {
            when {
                title.isEmpty() -> context.getString(R.string.error_title_empty)
                content.isEmpty() -> context.getString(R.string.error_content_empty)
                else -> context.getString(R.string.error_unknown)
            }

        }

    }

    fun onSearchViewQuerySubmit(context: Context?, query: String?) {
        context?.let {
            topicsRepo.getTopics(context, { topicList ->
                _topicManagementState.value =
                    TopicManagementState.LoadTopicList(topicList = topicList.filterByQuery(query = query))
            }, { error ->
                _topicManagementState.value =
                    TopicManagementState.RequestErrorReported(requestError = error)
            })

        }

    }

    fun onNavigationTopicsItemSelected() {
        _topicManagementState.value = TopicManagementState.NavigateToTopics
    }

    fun onNavigationLatestNewsItemSelected() {
        _topicManagementState.value = TopicManagementState.NavigateToLatestNews
    }

}

private fun List<Topic>.filterByQuery(query: String?): List<Topic> =
    query?.let { q ->
        filter { it.title.contains(other = q, ignoreCase = true) }
    } ?: run {
        this
    }
