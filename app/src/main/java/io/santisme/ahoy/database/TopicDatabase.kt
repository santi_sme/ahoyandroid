package io.santisme.ahoy.database

import androidx.room.*

@Entity
data class TopicEntity(
    @PrimaryKey(autoGenerate = true) val uid: Int = 0,
    @ColumnInfo(name = "topic_id") val topicId: String,
    @ColumnInfo(name = "topic_title") val title: String,
    @ColumnInfo(name = "topic_date") val date: String,
    @ColumnInfo(name = "topic_posts") val posts: Int,
    @ColumnInfo(name = "topic_views") val views: Int
)

@Dao
interface TopicDao {
    @Query(value = "SELECT * FROM TopicEntity")
    fun getTopics(): List<TopicEntity>

    @Query(value = "SELECT * FROM TopicEntity WHERE topic_id=:topicId")
    fun getTopicById(topicId: Int): List<TopicEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(topicList: List<TopicEntity>): List<Long>

    @Delete
    fun delete(topic: TopicEntity)
}

@Database(entities = [TopicEntity::class], version = 1)
abstract class TopicDatabase : RoomDatabase() {
    abstract fun topicDao(): TopicDao
}