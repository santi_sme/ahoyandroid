package io.santisme.ahoy.database

import androidx.room.*

@Entity
data class PostEntity(
    @PrimaryKey @ColumnInfo(name = "id") val id: String,
    @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "topic_id") val topicId: Int,
    @ColumnInfo(name = "topic_title") val topicTitle: String?,
    @ColumnInfo(name = "topic_slug") val topicSlug: String?,
    @ColumnInfo(name = "username") val username: String,
    @ColumnInfo(name = "avatar_template") val avatarTemplate: String,
    @ColumnInfo(name = "created_at") val createdAt: String,
    @ColumnInfo(name = "cooked") val cooked: String,
    @ColumnInfo(name = "post_number") val postNumber: Int?,
    @ColumnInfo(name = "reads") val reads: Int,
    @ColumnInfo(name = "score") val score: Double?
)

@Dao
interface PostDao {
    @Query(value = "SELECT * FROM PostEntity")
    fun getPosts(): List<PostEntity>

    @Query(value = "SELECT * FROM PostEntity WHERE id=:postId")
    fun getPostById(postId: Int): List<PostEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(postList: List<PostEntity>): List<Long>

    @Delete
    fun delete(post: PostEntity)
}

@Database(entities = [PostEntity::class], version = 1)
abstract class PostDatabase : RoomDatabase() {
    abstract fun postDao(): PostDao
}