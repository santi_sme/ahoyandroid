package io.santisme.ahoy.feature.login.presenter

import android.content.Context
import androidx.test.espresso.Espresso.closeSoftKeyboard
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import com.nhaarman.mockitokotlin2.*
import io.santisme.ahoy.R
import io.santisme.ahoy.data.repository.LoginRepository
import io.santisme.ahoy.domain.SignInModel
import io.santisme.ahoy.data.repository.UserRepo
import io.santisme.ahoy.feature.login.LoginContract
import io.santisme.ahoy.feature.login.view.ui.LoginActivity
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class LoginPresenterInstrumentationTest {

    @get:Rule
    val activityRule: ActivityTestRule<LoginActivity> =
        ActivityTestRule(LoginActivity::class.java, false, false)

    private lateinit var context: Context
    private lateinit var mockView: LoginContract.View
    private lateinit var mockRepo: LoginRepository
    private lateinit var loginPresenter: LoginContract.Presenter

    @Before
    fun setUp() {
        context = InstrumentationRegistry.getInstrumentation().targetContext
        mockView = mock()
        mockRepo = mock()
        loginPresenter = LoginPresenter(view = mockView, userRepo = mockRepo)
    }

    @After
    fun tearDown() {
    }

    @Test
    fun checkThatIfSignInDataAreIncorrect_showErrorIsInvoked() {
        // given
        val incorrectSignInModel =
            SignInModel(username = "", password = "")
        //when
        loginPresenter.onLoginClicked(context = context, model = incorrectSignInModel)
        //then
        verify(mockView).showError(msg = any())
    }

    @Test
    fun checkThatIfSignInDataAreCorrect_enableLoadingIsInvoked() {
        // given
        val correctSignInModel =
            SignInModel(username = "safdsfdsdfaass", password = "fsdfsdfdfsdfsdf")
        //when
        loginPresenter.onLoginClicked(context = context, model = correctSignInModel)
        //then
        verify(mockView).enableLoading(enable = any())
    }

    @Test
    fun checkThatIfUsernameIsFilledOutButPasswordRemainsEmpty_ErrorMessageIsDisplayed() {
        // given
        activityRule.launchActivity(null)
        UserRepo.logOut()
        val dummyUsername = "JohnDoe"

        //when
        onView(withId(R.id.inputUsername)).perform(typeText(dummyUsername))
        closeSoftKeyboard()
        onView(withId(R.id.buttonLogin)).perform(click())

        //then
        onView(withText(R.string.error_password_empty)).check(matches(isDisplayed()))
    }

    @Test
    fun checkThatIfUserIsLogged_LaunchTopicsActivityIsInvoked() {
        // given
        whenever(mockRepo.isLogged()).doReturn(true)
        // when
        loginPresenter.onViewCreatedWithNoSavedData()
        // then
        verify(mockView).launchTopicsActivity()
    }

    @Test
    fun checkThatIfUserIsNotLogged_NavigateToSignInIsInvoked() {
        // given
        whenever(mockRepo.isLogged()).doReturn(false)
        // when
        loginPresenter.onViewCreatedWithNoSavedData()
        // then
        verify(mockView).navigateToSignIn()
    }
}
