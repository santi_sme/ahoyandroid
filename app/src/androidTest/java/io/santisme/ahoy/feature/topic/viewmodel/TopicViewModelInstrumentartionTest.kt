package io.santisme.ahoy.feature.topic.viewmodel

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.nhaarman.mockitokotlin2.*
import io.santisme.ahoy.data.repository.TopicsRepository
import io.santisme.ahoy.domain.CreateTopicModel
import io.santisme.ahoy.domain.Topic
import io.santisme.ahoy.feature.topic.view.state.TopicManagementState
import org.junit.After
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class TopicViewModelInstrumentationTest {

    @get:Rule
    val rule: InstantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var viewModel: TopicViewModel
    private lateinit var context: Context
    private lateinit var mockRepo: TopicsRepository

    @Before
    fun setUp() {
        context = InstrumentationRegistry.getInstrumentation().targetContext
        mockRepo = mock()
        viewModel = TopicViewModel(topicsRepo = mockRepo)
    }

    @After
    fun tearDown() {
    }

    @Test
    fun sampleTest() {
        // given
        viewModel.topicManagementState

        // when
        viewModel.onTopicsFragmentResumed(context = context)

        // then
        viewModel.topicManagementState.observeForever { state ->
            assertTrue(state is TopicManagementState.ToggleCreateTopicLoading)
        }
    }

    @Test
    fun checkThatIfCreateTopicModelDataAreIncorrect_createTopicTopicFormErrorReportedIsInvoked() {
        // given
        viewModel.topicManagementState
        val incorrectCreateTopicModel =
            CreateTopicModel(title = "", content = "value context")

        // when
        viewModel.onCreateTopicOptionClicked(
            context = context,
            createTopicModel = incorrectCreateTopicModel
        )

        // then
        viewModel.topicManagementState.observeForever { state ->
            assertTrue(state is TopicManagementState.CreateTopicFormErrorReported)
        }
    }

    @Test
    fun checkThatIfCreateTopicModelDataAreValid_createTopicLoadingIsInvoked() {
        // given
        viewModel.topicManagementState
        val validCreateTopicModel = CreateTopicModel(
            title = "This is a Dummy title",
            content = "This is a dummy value context"
        )

        // when
        viewModel.onCreateTopicOptionClicked(
            context = context,
            createTopicModel = validCreateTopicModel
        )

        // then
        viewModel.topicManagementState.observeForever { state ->
            assertTrue(state is TopicManagementState.ToggleCreateTopicLoading)
        }
    }

    @Test
    fun checkThatIfOnViewCreatedWithNoSavedDataDetectsNoError_LoadTopicListIsInvoked() {
        // given
        viewModel.topicManagementState
        var argumentCaptor = argumentCaptor<(List<Topic>) -> Unit>()
        // when
        viewModel.onCreatedWithNoSavedData(context)
        // then
        verify(mockRepo).getTopics(eq(context), argumentCaptor.capture(), any())
        argumentCaptor.firstValue.invoke(emptyList())
        viewModel.topicManagementState.observeForever { state ->
            assertTrue(state is TopicManagementState.LoadTopicList)
        }
    }

    @Test
    fun checkThatIfOnCreateOptionClickedDetectsNoErrorAndModelsMatch_NavigateBackToTopicsInvoked() {
        // given
        viewModel.topicManagementState

        val createTopicModel = CreateTopicModel("Dummy title", "Dummy Content")
        var argumentCaptor = argumentCaptor<(CreateTopicModel) -> Unit>()

        // when
        viewModel.onCreateTopicOptionClicked(context, createTopicModel)
        // then
        verify(mockRepo).createTopic(
            eq(context),
            eq(createTopicModel),
            argumentCaptor.capture(),
            any()
        )
        argumentCaptor.firstValue.invoke(createTopicModel)
        viewModel.topicManagementState.observeForever { state ->
            assertTrue(state is TopicManagementState.NavigateBackToTopics)
        }
    }


}