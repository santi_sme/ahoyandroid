package io.santisme.ahoy

import io.santisme.ahoy.domain.LatestNews
import org.json.JSONObject
import org.junit.Assert.assertEquals
import org.junit.Test
import java.text.SimpleDateFormat
import java.util.*

class LatestNewsModelTest {
    @Test
    fun fromJson_isCorrect() {
        val stringLatestNews = "{\n" +
                "    \"latest_posts\": [\n" +
                "        {\n" +
                "            \"id\": 538,\n" +
                "            \"name\": \"Jacobo\",\n" +
                "            \"username\": \"jacobomd\",\n" +
                "            \"avatar_template\": \"/user_avatar/mdiscourse.keepcoding.io/jacobomd/{size}/35_2.png\",\n" +
                "            \"created_at\": \"2020-02-16T10:53:10.821Z\",\n" +
                "            \"cooked\": \"<p>Contenido de Este topic para probar ultimas implementaciones.</p>\",\n" +
                "            \"post_number\": 1,\n" +
                "            \"post_type\": 1,\n" +
                "            \"updated_at\": \"2020-02-16T10:53:10.821Z\",\n" +
                "            \"reply_count\": 0,\n" +
                "            \"reply_to_post_number\": null,\n" +
                "            \"quote_count\": 0,\n" +
                "            \"incoming_link_count\": 0,\n" +
                "            \"reads\": 1,\n" +
                "            \"score\": 0.2,\n" +
                "            \"yours\": false,\n" +
                "            \"topic_id\": 258,\n" +
                "            \"topic_slug\": \"penultimo-topic-android-avanzado\",\n" +
                "            \"topic_title\": \"Penultimo topic Android avanzado\",\n" +
                "            \"topic_html_title\": \"Penultimo topic Android avanzado\",\n" +
                "            \"category_id\": 1,\n" +
                "            \"display_username\": \"Jacobo\",\n" +
                "            \"primary_group_name\": null,\n" +
                "            \"primary_group_flair_url\": null,\n" +
                "            \"primary_group_flair_bg_color\": null,\n" +
                "            \"primary_group_flair_color\": null,\n" +
                "            \"version\": 1,\n" +
                "            \"can_edit\": false,\n" +
                "            \"can_delete\": false,\n" +
                "            \"can_recover\": false,\n" +
                "            \"can_wiki\": false,\n" +
                "            \"user_title\": null,\n" +
                "            \"raw\": \"Contenido de Este topic para probar ultimas implementaciones.\",\n" +
                "            \"actions_summary\": [\n" +
                "                {\n" +
                "                    \"id\": 2,\n" +
                "                    \"can_act\": true\n" +
                "                },\n" +
                "                {\n" +
                "                    \"id\": 3,\n" +
                "                    \"can_act\": true\n" +
                "                },\n" +
                "                {\n" +
                "                    \"id\": 4,\n" +
                "                    \"can_act\": true\n" +
                "                },\n" +
                "                {\n" +
                "                    \"id\": 8,\n" +
                "                    \"can_act\": true\n" +
                "                },\n" +
                "                {\n" +
                "                    \"id\": 6,\n" +
                "                    \"can_act\": true\n" +
                "                },\n" +
                "                {\n" +
                "                    \"id\": 7,\n" +
                "                    \"can_act\": true\n" +
                "                }\n" +
                "            ],\n" +
                "            \"moderator\": false,\n" +
                "            \"admin\": false,\n" +
                "            \"staff\": false,\n" +
                "            \"user_id\": 12,\n" +
                "            \"hidden\": false,\n" +
                "            \"trust_level\": 1,\n" +
                "            \"deleted_at\": null,\n" +
                "            \"user_deleted\": false,\n" +
                "            \"edit_reason\": null,\n" +
                "            \"can_view_edit_history\": true,\n" +
                "            \"wiki\": false\n" +
                "        }" +
                "]}"
        val json = JSONObject(stringLatestNews)
        val postList = LatestNews.parseLatestNews(json)
        val post = postList.first()

        val parser = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault())
        val formatter = SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.getDefault())
        val dateFormatted = formatter.format(parser.parse(post.createdAt))

        assertEquals("538", post.id)
        assertEquals("Jacobo", post.name)
        assertEquals("jacobomd", post.username)
        assertEquals(
            "/user_avatar/mdiscourse.keepcoding.io/jacobomd/{size}/35_2.png",
            post.avatarTemplate
        )
        assertEquals("16/02/2020 10:53:10", dateFormatted)
        assertEquals("Contenido de Este topic para probar ultimas implementaciones.",
            post.cooked
        )
        assertEquals(1, post.postNumber)
        assertEquals(1, post.reads)
    }

    private fun formatDate(date: String): Date {
        val formatter = SimpleDateFormat("dd/MM/yyyy hh:mm:ss")

        return formatter.parse(date)
            ?: throw IllegalArgumentException("Date $date has incorrect format, try again with the following: dd/MM/yyyy hh:mm:ss")
    }
}