package io.santisme.ahoy

import io.santisme.ahoy.domain.SignUpModel
import junit.framework.Assert.assertEquals
import org.junit.Test

class SignUpModelTest {
    @Test
    fun toJson_correct() {
        val model = SignUpModel(
            "test",
            "test@test.com",
            "testpassword"
        )

        val json = model.toJson()

        assertEquals("test", json["name"])
        assertEquals("test", json["username"])
        assertEquals("test@test.com", json["email"])
        assertEquals("testpassword", json["password"])
        assertEquals(true, json["active"])
        assertEquals(true, json["approved"])

    }
}