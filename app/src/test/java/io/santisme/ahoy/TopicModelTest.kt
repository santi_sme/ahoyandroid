package io.santisme.ahoy

import io.santisme.ahoy.domain.Topic
import org.json.JSONObject
import org.junit.Assert.assertEquals
import org.junit.Test
import java.text.SimpleDateFormat
import java.util.*

class TopicModelTest {
    @Test
    fun getOffset_year_isCorrect() {
        val dateToCompare = formatDate("01/01/2020 10:00:00")
        val testTopic = Topic(
            title = "Test",
            date = "2017-01-01T10:00:00"
        )

        val offset = testTopic.getTimeOffset(dateToCompare)
        assertEquals("Amount comparison", 3, offset.amount)
        assertEquals("Unit comparison", Calendar.YEAR, offset.unit)

    }

    @Test
    fun getOffset_month_isCorrect() {
        val dateToCompare = formatDate("01/02/2019 10:00:00")
        val testTopic = Topic(
            title = "Test",
            date = "2019-01-01T10:00:00"
        )

        val offset = testTopic.getTimeOffset(dateToCompare)
        assertEquals("Amount comparison", 1, offset.amount)
        assertEquals("Unit comparison", Calendar.MONTH, offset.unit)

    }

    @Test
    fun getOffset_day_isCorrect() {
        val dateToCompare = formatDate("02/02/2019 10:00:00")

        val testTopic = Topic(
            title = "Test",
            date = "2019-02-01T10:00:00"
        )

        val offset = testTopic.getTimeOffset(dateToCompare)
        assertEquals("Amount comparison", 1, offset.amount)
        assertEquals("Unit comparison", Calendar.DAY_OF_MONTH, offset.unit)

    }

    @Test
    fun getOffset_hour_isCorrect() {
        val dateToCompare = formatDate("02/02/2019 11:00:00")
        val testTopic = Topic(
            title = "Test",
            date = "2019-02-02T10:00:00"
        )

        val offset = testTopic.getTimeOffset(dateToCompare)
        assertEquals("Amount comparison", 1, offset.amount)
        assertEquals("Unit comparison", Calendar.HOUR, offset.unit)

    }

    @Test
    fun getOffset_minute_isCorrect() {
        val dateToCompare = formatDate("02/02/2019 10:01:00")
        val testTopic = Topic(
            title = "Test",
            date = "2019-02-02T10:00:00"
        )

        val offset = testTopic.getTimeOffset(dateToCompare)
        assertEquals("Amount comparison", 1, offset.amount)
        assertEquals("Unit comparison", Calendar.MINUTE, offset.unit)

    }

    @Test
    fun getOffset_seconds_isCorrect() {
        val dateToCompare = formatDate("02/02/2019 10:00:30")
        val testTopic = Topic(
            title = "Test",
            date = "2019-02-02T10:00:00"
        )

        val offset = testTopic.getTimeOffset(dateToCompare)
        assertEquals("Amount comparison", 0, offset.amount)
        assertEquals("Unit comparison", Calendar.MINUTE, offset.unit)

    }

    @Test
    fun fromJson_isCorrect() {
        val stringTopic =
            "{\"id\":199,\"title\":\"Topic creado por jacobomd 11\",\"fancy_title\":\"Topic creado por jacobomd 11\",\"slug\":\"topic-creado-por-jacobomd-11\",\"posts_count\":6,\"reply_count\":0,\"highest_post_number\":6,\"image_url\":null,\"created_at\":\"2019-11-11T11:32:44.770Z\",\"last_posted_at\":\"2020-01-14T11:52:54.233Z\",\"bumped\":true,\"bumped_at\":\"2020-01-14T11:52:54.233Z\",\"unseen\":false,\"pinned\":false,\"unpinned\":null,\"visible\":true,\"closed\":false,\"archived\":false,\"bookmarked\":null,\"liked\":null,\"views\":11,\"like_count\":0,\"has_summary\":false,\"archetype\":\"regular\",\"last_poster_username\":\"jacobomd\",\"category_id\":1,\"pinned_globally\":false,\"featured_link\":null,\"posters\":[{\"extras\":\"latest\",\"description\":\"Original Poster, Most Recent Poster\",\"user_id\":12,\"primary_group_id\":null},{\"extras\":null,\"description\":\"Frequent Poster\",\"user_id\":8,\"primary_group_id\":null}]}"
        val json = JSONObject(stringTopic)
        val topic = Topic.parseTopic(json)

        val parser = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault())
        val formatter = SimpleDateFormat("dd/MM/yyyy hh:mm:ss")
        val dateFormatted = formatter.format(parser.parse(topic.date))

        assertEquals("199", topic.id)
        assertEquals("Topic creado por jacobomd 11", topic.title)
        assertEquals(6, topic.posts)
        assertEquals(11, topic.views)
        assertEquals("11/11/2019 11:32:44", dateFormatted)
    }

    private fun formatDate(date: String): Date {
        val formatter = SimpleDateFormat("dd/MM/yyyy hh:mm:ss")

        return formatter.parse(date)
            ?: throw IllegalArgumentException("Date $date has incorrect format, try again with the following: dd/MM/yyyy hh:mm:ss")
    }
}