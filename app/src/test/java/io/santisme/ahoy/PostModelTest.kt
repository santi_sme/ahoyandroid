package io.santisme.ahoy

import io.santisme.ahoy.domain.Post
import org.json.JSONObject
import org.junit.Test
import org.junit.Assert.*
import java.lang.IllegalArgumentException
import java.text.SimpleDateFormat
import java.util.*

class PostModelTest {
    @Test
    fun fromJson_isCorrect() {
        val stringTopic = "{\"id\":10,\"name\":\"system\",\"username\":\"system\",\"avatar_template\":\"/user_avatar/mdiscourse.keepcoding.io/system/{size}/1_2.png\",\"created_at\":\"2019-07-03T18:15:04.011Z\",\"cooked\":\"\\u003cp\\u003eDiscourse Setup\\u003cbr\\u003e\\nThe first paragraph of this pinned topic will be visible as a welcome message to all new visitors on your homepage. It’s important!\\u003c/p\\u003e\\n\\u003cp\\u003e\\u003cstrong\\u003eEdit this\\u003c/strong\\u003e into a brief description of your community:\\u003c/p\\u003e\\n\\u003cul\\u003e\\n\\u003cli\\u003eWho is it for?\\u003c/li\\u003e\\n\\u003cli\\u003eWhat can they find here?\\u003c/li\\u003e\\n\\u003cli\\u003eWhy should they come here?\\u003c/li\\u003e\\n\\u003cli\\u003eWhere can they read more (links, resources, etc)?\\u003c/li\\u003e\\n\\u003c/ul\\u003e\\n\\u003cp\\u003e\\u003cimg src=\\\"//mdiscourse.keepcoding.io/uploads/default/original/1X/1b44248c86c19414b907bf3b799ee95ae1681e68.gif\\\" alt data-base62-sha1=\\\"3TcSZhWVxDVYhEESC9aWjS1t6Sk\\\" width=\\\"508\\\" height=\\\"106\\\"\\u003e\\u003c/p\\u003e\\n\\u003cp\\u003eYou may want to close this topic via the admin \\u003cimg src=\\\"//mdiscourse.keepcoding.io/images/emoji/twitter/wrench.png?v=9\\\" title=\\\":wrench:\\\" class=\\\"emoji\\\" alt=\\\":wrench:\\\"\\u003e (at the upper right and bottom), so that replies don’t pile up on an announcement.\\u003c/p\\u003e\",\"post_number\":1,\"post_type\":1,\"updated_at\":\"2019-07-03T19:13:57.641Z\",\"reply_count\":0,\"reply_to_post_number\":null,\"quote_count\":0,\"incoming_link_count\":1,\"reads\":7,\"score\":6.4,\"yours\":false,\"topic_id\":7,\"topic_slug\":\"welcome-to-discourse\",\"display_username\":\"system\",\"primary_group_name\":null,\"primary_group_flair_url\":null,\"primary_group_flair_bg_color\":null,\"primary_group_flair_color\":null,\"version\":3,\"can_edit\":false,\"can_delete\":false,\"can_recover\":false,\"can_wiki\":false,\"read\":true,\"user_title\":null,\"actions_summary\":[],\"moderator\":true,\"admin\":true,\"staff\":true,\"user_id\":-1,\"hidden\":false,\"trust_level\":4,\"deleted_at\":null,\"user_deleted\":false,\"edit_reason\":\"downloaded local copies of images\",\"can_view_edit_history\":true,\"wiki\":false}"
        val json = JSONObject(stringTopic)
        val post = Post.parsePost(json)

        val formatter = SimpleDateFormat("dd/MM/yyyy hh:mm:ss", Locale.getDefault())
        val dateFormatted = formatter.format(post.createdAt)

        assertEquals("10", post.id)
        assertEquals("system", post.name)
        assertEquals("system", post.username)
        assertEquals("/user_avatar/mdiscourse.keepcoding.io/system/{size}/1_2.png", post.avatarTemplate)
        assertEquals("03/07/2019 08:15:04", dateFormatted)
        assertEquals("<p>Discourse Setup<br>\n" +
                "The first paragraph of this pinned topic will be visible as a welcome message to all new visitors on your homepage. It’s important!</p>\n" +
                "<p><strong>Edit this</strong> into a brief description of your community:</p>\n" +
                "<ul>\n" +
                "<li>Who is it for?</li>\n" +
                "<li>What can they find here?</li>\n" +
                "<li>Why should they come here?</li>\n" +
                "<li>Where can they read more (links, resources, etc)?</li>\n" +
                "</ul>\n" +
                "<p><img src=\"//mdiscourse.keepcoding.io/uploads/default/original/1X/1b44248c86c19414b907bf3b799ee95ae1681e68.gif\" alt data-base62-sha1=\"3TcSZhWVxDVYhEESC9aWjS1t6Sk\" width=\"508\" height=\"106\"></p>\n" +
                "<p>You may want to close this topic via the admin <img src=\"//mdiscourse.keepcoding.io/images/emoji/twitter/wrench.png?v=9\" title=\":wrench:\" class=\"emoji\" alt=\":wrench:\"> (at the upper right and bottom), so that replies don’t pile up on an announcement.</p>", post.cooked)
        assertEquals(1, post.postNumber)
        assertEquals(7, post.reads)
    }

    private fun formatDate(date: String): Date {
        val formatter = SimpleDateFormat("dd/MM/yyyy hh:mm:ss")

        return formatter.parse(date)
            ?: throw IllegalArgumentException("Date $date has incorrect format, try again with the following: dd/MM/yyyy hh:mm:ss")
    }
}